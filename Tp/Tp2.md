Tp-Léo

# B2 Réseau 2019 - TP2: Network low-level, Switching

## Sommaire

* [I. Simplest Setup](#i-simplest-setup)
  * [Topologie](#topologie)
  * [Plan D'adressage](#plan-dadressage)
  * [Do](#do)
* [II. More Switches](#ii-more-switches)
  * [Topologie](#topologie-1)
  * [Plan D'adressage](#plan-dadressage-1)
  * [Do](#do-1)
  * [Mise en évidence du Spanning Tree Protocol](#mise-en-évidence-du-spanning-tree-protocol)
* [III. Isolation](#iii-isolation)
  * [1. Simple](#1-simple)
    * [Topologie](#topologie-2)
    * [Plan D'adressage](#plan-dadressage-2)
    * [Do](#do-2)
  * [2. Avec Trunk](#2-avec-trunk)
    * [Topologie](#topologie-3)
    * [Plan D'adressage](#plan-dadressage-3)
    * [Do](#do-3)
* [IV. Need Perfs](#iv-need-perfs)
  * [Topologie](#topologie-4)
  * [Plan D'adressage](#plan-dadressage-4)
  * [Do](#do-4)

# I. Simplest Setup

### 1. Topologie

```
+-----+        +-------+        +-----+
| PC1 +--------+  SW1  +--------+ PC2 |
+-----+        +-------+        +-----+
```

### 2. Plan D'adressage

| Machine | Net 1 |
| Pc 1 | 10.2.1.1/24 |
| Pc 2 | 10.2.1.2/24 |

### 3. Do

Une fois les Vpcs configuré nous pouvons les ping entre eux. 

```bash
PC1> ping 10.2.1.2
84 bytes from 10.2.1.2 icmp_seq=1 ttl=64 time=0.502 ms
84 bytes from 10.2.1.2 icmp_seq=2 ttl=64 time=1.007 ms
84 bytes from 10.2.1.2 icmp_seq=3 ttl=64 time=0.675 ms
84 bytes from 10.2.1.2 icmp_seq=4 ttl=64 time=1.590 ms
84 bytes from 10.2.1.2 icmp_seq=5 ttl=64 time=0.803 ms

PC2> ping 10.2.1.1
84 bytes from 10.2.1.1 icmp_seq=1 ttl=64 time=0.380 ms
84 bytes from 10.2.1.1 icmp_seq=2 ttl=64 time=0.680 ms
84 bytes from 10.2.1.1 icmp_seq=3 ttl=64 time=0.569 ms
84 bytes from 10.2.1.1 icmp_seq=4 ttl=64 time=0.484 ms
84 bytes from 10.2.1.1 icmp_seq=5 ttl=64 time=0.483 ms
```

Pour savoir comment cela ce déroule nous lançons Wireshark en ayant vidé les tables ARP.

```bash
No.     Time           Source                Destination           Protocol Length Info
     78 105.182486     Private_66:68:00      Broadcast             ARP      64     Gratuitous ARP for 10.2.1.1 (Request)

Frame 78: 64 bytes on wire (512 bits), 64 bytes captured (512 bits) on interface 0
Ethernet II, Src: Private_66:68:00 (00:50:79:66:68:00), Dst: Broadcast (ff:ff:ff:ff:ff:ff)
Address Resolution Protocol (request/gratuitous ARP)

No.     Time           Source                Destination           Protocol Length Info
     79 106.183542     Private_66:68:00      Broadcast             ARP      64     Gratuitous ARP for 10.2.1.1 (Request)

Frame 79: 64 bytes on wire (512 bits), 64 bytes captured (512 bits) on interface 0
Ethernet II, Src: Private_66:68:00 (00:50:79:66:68:00), Dst: Broadcast (ff:ff:ff:ff:ff:ff)
Address Resolution Protocol (request/gratuitous ARP)

No.     Time           Source                Destination           Protocol Length Info
     81 107.183725     Private_66:68:00      Broadcast             ARP      64     Gratuitous ARP for 10.2.1.1 (Request)

Frame 81: 64 bytes on wire (512 bits), 64 bytes captured (512 bits) on interface 0
Ethernet II, Src: Private_66:68:00 (00:50:79:66:68:00), Dst: Broadcast (ff:ff:ff:ff:ff:ff)
Address Resolution Protocol (request/gratuitous ARP)

No.     Time           Source                Destination           Protocol Length Info
     86 111.564409     Private_66:68:01      Broadcast             ARP      64     Gratuitous ARP for 10.2.1.2 (Request)

Frame 86: 64 bytes on wire (512 bits), 64 bytes captured (512 bits) on interface 0
Ethernet II, Src: Private_66:68:01 (00:50:79:66:68:01), Dst: Broadcast (ff:ff:ff:ff:ff:ff)
Address Resolution Protocol (request/gratuitous ARP)

No.     Time           Source                Destination           Protocol Length Info
     87 111.564983     Private_66:68:01      Broadcast             ARP      64     Gratuitous ARP for 10.2.1.2 (Request)

Frame 87: 64 bytes on wire (512 bits), 64 bytes captured (512 bits) on interface 0
Ethernet II, Src: Private_66:68:01 (00:50:79:66:68:01), Dst: Broadcast (ff:ff:ff:ff:ff:ff)
Address Resolution Protocol (request/gratuitous ARP)

No.     Time           Source                Destination           Protocol Length Info
     89 112.564240     Private_66:68:01      Broadcast             ARP      64     Gratuitous ARP for 10.2.1.2 (Request)

Frame 89: 64 bytes on wire (512 bits), 64 bytes captured (512 bits) on interface 0
Ethernet II, Src: Private_66:68:01 (00:50:79:66:68:01), Dst: Broadcast (ff:ff:ff:ff:ff:ff)
Address Resolution Protocol (request/gratuitous ARP)

No.     Time           Source                Destination           Protocol Length Info
     90 112.564313     Private_66:68:01      Broadcast             ARP      64     Gratuitous ARP for 10.2.1.2 (Request)

Frame 90: 64 bytes on wire (512 bits), 64 bytes captured (512 bits) on interface 0
Ethernet II, Src: Private_66:68:01 (00:50:79:66:68:01), Dst: Broadcast (ff:ff:ff:ff:ff:ff)
Address Resolution Protocol (request/gratuitous ARP)

No.     Time           Source                Destination           Protocol Length Info
     91 113.565672     Private_66:68:01      Broadcast             ARP      64     Gratuitous ARP for 10.2.1.2 (Request)

Frame 91: 64 bytes on wire (512 bits), 64 bytes captured (512 bits) on interface 0
Ethernet II, Src: Private_66:68:01 (00:50:79:66:68:01), Dst: Broadcast (ff:ff:ff:ff:ff:ff)
Address Resolution Protocol (request/gratuitous ARP)

No.     Time           Source                Destination           Protocol Length Info
     92 113.565744     Private_66:68:01      Broadcast             ARP      64     Gratuitous ARP for 10.2.1.2 (Request)

Frame 92: 64 bytes on wire (512 bits), 64 bytes captured (512 bits) on interface 0
Ethernet II, Src: Private_66:68:01 (00:50:79:66:68:01), Dst: Broadcast (ff:ff:ff:ff:ff:ff)
Address Resolution Protocol (request/gratuitous ARP)
```
On remarque directement qu'il y a eu un échange ARP pour qu'ils puissent discuter ensemble. Cet échange se fait si les deux pc n'ont jamais communiqué auparavant ensemble. Sinon il est marqué dans sa table ARP et n'a donc pas besoin de faire cette demande.

Le switch quand à lui ne possèble pas d'ip, il sert seulement de relai au deux machines. Elles ont besoin d'une ip pour pourvoir communiquer entre elles, c'est comme une adresse postale, c'est nécessaire pour pouvoir se repérer au yeux du monde extérieur.

# II. More Switches 

### 1. Topologie

```
                        +-----+
                        | PC2 |
                        +--+--+
                           |
                           |
                       +---+---+
                   +---+  SW2  +----+
                   |   +-------+    |
                   |                |
                   |                |
+-----+        +---+---+        +---+---+        +-----+
| PC1 +--------+  SW1  +--------+  SW3  +--------+ PC3 |
+-----+        +-------+        +-------+        +-----+
```

### 2. Plan D'adressage

| Machine | Net 1 |
| Pc 1 | 10.2.2.1/24 |
| Pc 2 | 10.2.2.2/24 |
| Pc 3 | 10.2.2.3/24 |

### 3. Do

```bash
Pc 1:
PC1> ping 10.2.2.2 -c 2
84 bytes from 10.2.2.2 icmp_seq=1 ttl=64 time=1.240 ms
84 bytes from 10.2.2.2 icmp_seq=2 ttl=64 time=1.065 ms

PC1> ping 10.2.2.3 -c 2
84 bytes from 10.2.2.3 icmp_seq=1 ttl=64 time=1.235 ms
84 bytes from 10.2.2.3 icmp_seq=2 ttl=64 time=0.963 ms

Pc 2:
PC2> ping 10.2.2.1 -c 2
84 bytes from 10.2.2.1 icmp_seq=1 ttl=64 time=1.832 ms
84 bytes from 10.2.2.1 icmp_seq=2 ttl=64 time=1.420 ms

PC2> ping 10.2.2.3 -c 2
84 bytes from 10.2.2.3 icmp_seq=1 ttl=64 time=2.581 ms
84 bytes from 10.2.2.3 icmp_seq=2 ttl=64 time=1.265 ms

Pc 3:
PC3> ping 10.2.2.1 -c 2
84 bytes from 10.2.2.1 icmp_seq=1 ttl=64 time=1.003 ms
84 bytes from 10.2.2.1 icmp_seq=2 ttl=64 time=0.754 ms

PC3> ping 10.2.2.2 -c 2
84 bytes from 10.2.2.2 icmp_seq=1 ttl=64 time=1.863 ms
84 bytes from 10.2.2.2 icmp_seq=2 ttl=64 time=2.256 ms
```
Lorsque la commande `show mac address-table` est exécuté nous obtenons un résultat similaire à celui-ci,

```bash
IOU1#show mac address-table
          Mac Address Table
-------------------------------------------

Vlan    Mac Address       Type        Ports
----    -----------       --------    -----
   1    0050.7966.6800    DYNAMIC     Et0/0
   1    0050.7966.6801    DYNAMIC     Et0/2
   1    0050.7966.6802    DYNAMIC     Et0/1
   1    aabb.cc00.0200    DYNAMIC     Et0/2
   1    aabb.cc00.0310    DYNAMIC     Et0/1
   1    aabb.cc00.0320    DYNAMIC     Et0/2
Total Mac Addresses for this criterion: 6
```
Chaque ligne indiquent une addresse mac connu de la machine (Switch ici), ainsi que le port qu'elle utilise pour communiquer avec elle.

Il est facile de remarquer lorsque nous lançons une capture Wireshar, des trames CDP apparaissent.

```bash
No.     Time           Source                Destination           Protocol Length Info
     32 36.820902      Private_66:68:00      Broadcast             ARP      64     Who has 10.2.2.3? Tell 10.2.2.1

Frame 32: 64 bytes on wire (512 bits), 64 bytes captured (512 bits) on interface 0
Ethernet II, Src: Private_66:68:00 (00:50:79:66:68:00), Dst: Broadcast (ff:ff:ff:ff:ff:ff)
Address Resolution Protocol (request)

No.     Time           Source                Destination           Protocol Length Info
     33 36.820990      Private_66:68:00      Broadcast             ARP      64     Who has 10.2.2.3? Tell 10.2.2.1

Frame 33: 64 bytes on wire (512 bits), 64 bytes captured (512 bits) on interface 0
Ethernet II, Src: Private_66:68:00 (00:50:79:66:68:00), Dst: Broadcast (ff:ff:ff:ff:ff:ff)
Address Resolution Protocol (request)

No.     Time           Source                Destination           Protocol Length Info
     39 46.133786      aa:bb:cc:00:02:00     CDP/VTP/DTP/PAgP/UDLD CDP      456    Device ID: IOU2  Port ID: Ethernet0/0  

Frame 39: 456 bytes on wire (3648 bits), 456 bytes captured (3648 bits) on interface 0
IEEE 802.3 Ethernet 
Logical-Link Control
Cisco Discovery Protocol

No.     Time           Source                Destination           Protocol Length Info
     46 58.611145      aa:bb:cc:00:01:20     CDP/VTP/DTP/PAgP/UDLD CDP      456    Device ID: IOU1  Port ID: Ethernet0/2  

Frame 46: 456 bytes on wire (3648 bits), 456 bytes captured (3648 bits) on interface 0
IEEE 802.3 Ethernet 
Logical-Link Control
Cisco Discovery Protocol
```
Comme je ne connaissais pas du tout la trame je me suis renseigné et ce sont des trames spécifique au switch Cisco, elles s'envoient toute les 60 secondes et transporte des informations sur les propriétés du port, des addresses de protocole ...

### 4. Mise en évidence du Spanning Tree Protocol

Des trois swicth c'est le premier qui est le *Root bridge* comme nous le montre la commande `show spanning-tree`. 
```bash
IOU1#show spanning-tree 
VLAN0001
  Spanning tree enabled protocol rstp
  Root ID    Priority    32769
             Address     aabb.cc00.0100
             This bridge is the root

Pour IOU2:   Cost        100
             Port        1 (Ethernet0/0)
Pour IOU3:   Cost        100
             Port        2 (Ethernet0/1)

             Hello Time   2 sec  Max Age 20 sec  Forward Delay 15 sec

  Bridge ID  :Priority    32769  (priority 32768 sys-id-ext 1)
             Address     aabb.cc00.0100
             Hello Time   2 sec  Max Age 20 sec  Forward Delay 15 sec
             Aging Time  300 sec

Interface           Role Sts Cost      Prio.Nbr Type
------------------- ---- --- --------- -------- --------------------------------
Et0/0               Desg FWD 100       128.1    Shr 
Et0/1               Desg FWD 100       128.2    Shr 
Et0/2               Desg FWD 100       128.3    Shr 
Et0/3               Desg FWD 100       128.4    Shr 
Et1/0               Desg FWD 100       128.5    Shr 
Et1/1               Desg FWD 100       128.6    Shr 
Et1/2               Desg FWD 100       128.7    Shr 
Et1/3               Desg FWD 100       128.8    Shr 
Et2/0               Desg FWD 100       128.9    Shr 
Et2/1               Desg FWD 100       128.10   Shr 
Et2/2               Desg FWD 100       128.11   Shr 
Et2/3               Desg FWD 100       128.12   Shr 
Et3/0               Desg FWD 100       128.13   Shr 
Et3/1               Desg FWD 100       128.14   Shr 
Et3/2               Desg FWD 100       128.15   Shr 
Et3/3               Desg FWD 100       128.16   Shr 
```

Ping du Pc3 vers le Pc2
```bash
PC3> ping 10.2.2.2
84 bytes from 10.2.2.2 icmp_seq=1 ttl=64 time=0.700 ms
84 bytes from 10.2.2.2 icmp_seq=2 ttl=64 time=0.879 ms
84 bytes from 10.2.2.2 icmp_seq=3 ttl=64 time=0.842 ms
84 bytes from 10.2.2.2 icmp_seq=4 ttl=64 time=1.077 ms
84 bytes from 10.2.2.2 icmp_seq=5 ttl=64 time=0.808 ms
```

<details>
<summary>IOU1 > IOU2</summary>

```bash
No.     Time           Source                Destination           Protocol Length Info
     28 38.008589      Private_66:68:02      Broadcast             ARP      64     Who has 10.2.2.2? Tell 10.2.2.3

Frame 28: 64 bytes on wire (512 bits), 64 bytes captured (512 bits) on interface 0
Ethernet II, Src: Private_66:68:02 (00:50:79:66:68:02), Dst: Broadcast (ff:ff:ff:ff:ff:ff)
Address Resolution Protocol (request)

No.     Time           Source                Destination           Protocol Length Info
     29 38.008636      Private_66:68:02      Broadcast             ARP      64     Who has 10.2.2.2? Tell 10.2.2.3

Frame 29: 64 bytes on wire (512 bits), 64 bytes captured (512 bits) on interface 0
Ethernet II, Src: Private_66:68:02 (00:50:79:66:68:02), Dst: Broadcast (ff:ff:ff:ff:ff:ff)
Address Resolution Protocol (request)

No.     Time           Source                Destination           Protocol Length Info
     30 38.013063      Private_66:68:01      Private_66:68:02      ARP      64     10.2.2.2 is at 00:50:79:66:68:01

Frame 30: 64 bytes on wire (512 bits), 64 bytes captured (512 bits) on interface 0
Ethernet II, Src: Private_66:68:01 (00:50:79:66:68:01), Dst: Private_66:68:02 (00:50:79:66:68:02)
Address Resolution Protocol (reply)

No.     Time           Source                Destination           Protocol Length Info
     31 38.013477      Private_66:68:01      Private_66:68:02      ARP      64     10.2.2.2 is at 00:50:79:66:68:01

Frame 31: 64 bytes on wire (512 bits), 64 bytes captured (512 bits) on interface 0
Ethernet II, Src: Private_66:68:01 (00:50:79:66:68:01), Dst: Private_66:68:02 (00:50:79:66:68:02)
Address Resolution Protocol (reply)

No.     Time           Source                Destination           Protocol Length Info
     32 38.013749      Private_66:68:01      Private_66:68:02      ARP      64     10.2.2.2 is at 00:50:79:66:68:01

Frame 32: 64 bytes on wire (512 bits), 64 bytes captured (512 bits) on interface 0
Ethernet II, Src: Private_66:68:01 (00:50:79:66:68:01), Dst: Private_66:68:02 (00:50:79:66:68:02)
Address Resolution Protocol (reply)

No.     Time           Source                Destination           Protocol Length Info
     33 38.014009      Private_66:68:01      Private_66:68:02      ARP      64     10.2.2.2 is at 00:50:79:66:68:01

Frame 33: 64 bytes on wire (512 bits), 64 bytes captured (512 bits) on interface 0
Ethernet II, Src: Private_66:68:01 (00:50:79:66:68:01), Dst: Private_66:68:02 (00:50:79:66:68:02)
Address Resolution Protocol (reply)

No.     Time           Source                Destination           Protocol Length Info
     34 38.014268      Private_66:68:01      Private_66:68:02      ARP      64     10.2.2.2 is at 00:50:79:66:68:01

Frame 34: 64 bytes on wire (512 bits), 64 bytes captured (512 bits) on interface 0
Ethernet II, Src: Private_66:68:01 (00:50:79:66:68:01), Dst: Private_66:68:02 (00:50:79:66:68:02)
Address Resolution Protocol (reply)

No.     Time           Source                Destination           Protocol Length Info
     35 38.014550      Private_66:68:01      Private_66:68:02      ARP      64     10.2.2.2 is at 00:50:79:66:68:01

Frame 35: 64 bytes on wire (512 bits), 64 bytes captured (512 bits) on interface 0
Ethernet II, Src: Private_66:68:01 (00:50:79:66:68:01), Dst: Private_66:68:02 (00:50:79:66:68:02)
Address Resolution Protocol (reply)

No.     Time           Source                Destination           Protocol Length Info
     36 38.014813      Private_66:68:01      Private_66:68:02      ARP      64     10.2.2.2 is at 00:50:79:66:68:01

Frame 36: 64 bytes on wire (512 bits), 64 bytes captured (512 bits) on interface 0
Ethernet II, Src: Private_66:68:01 (00:50:79:66:68:01), Dst: Private_66:68:02 (00:50:79:66:68:02)
Address Resolution Protocol (reply)

No.     Time           Source                Destination           Protocol Length Info
     37 38.015138      Private_66:68:01      Private_66:68:02      ARP      64     10.2.2.2 is at 00:50:79:66:68:01

Frame 37: 64 bytes on wire (512 bits), 64 bytes captured (512 bits) on interface 0
Ethernet II, Src: Private_66:68:01 (00:50:79:66:68:01), Dst: Private_66:68:02 (00:50:79:66:68:02)
Address Resolution Protocol (reply)

No.     Time           Source                Destination           Protocol Length Info
     38 38.015658      10.2.2.3              10.2.2.2              ICMP     98     Echo (ping) request  id=0x648c, seq=1/256, ttl=64 (reply in 39)

Frame 38: 98 bytes on wire (784 bits), 98 bytes captured (784 bits) on interface 0
Ethernet II, Src: Private_66:68:02 (00:50:79:66:68:02), Dst: Private_66:68:01 (00:50:79:66:68:01)
Internet Protocol Version 4, Src: 10.2.2.3, Dst: 10.2.2.2
Internet Control Message Protocol

No.     Time           Source                Destination           Protocol Length Info
     39 38.016095      10.2.2.2              10.2.2.3              ICMP     98     Echo (ping) reply    id=0x648c, seq=1/256, ttl=64 (request in 38)

Frame 39: 98 bytes on wire (784 bits), 98 bytes captured (784 bits) on interface 0
Ethernet II, Src: Private_66:68:01 (00:50:79:66:68:01), Dst: Private_66:68:02 (00:50:79:66:68:02)
Internet Protocol Version 4, Src: 10.2.2.2, Dst: 10.2.2.3
Internet Control Message Protocol

No.     Time           Source                Destination           Protocol Length Info
     40 38.141493      aa:bb:cc:00:01:10     Spanning-tree-(for-bridges)_00 STP      60     RST. Root = 32768/1/aa:bb:cc:00:01:00  Cost = 0  Port = 0x8002

Frame 40: 60 bytes on wire (480 bits), 60 bytes captured (480 bits) on interface 0
IEEE 802.3 Ethernet
Logical-Link Control
Spanning Tree Protocol

No.     Time           Source                Destination           Protocol Length Info
     41 39.016872      10.2.2.3              10.2.2.2              ICMP     98     Echo (ping) request  id=0x658c, seq=2/512, ttl=64 (reply in 42)

Frame 41: 98 bytes on wire (784 bits), 98 bytes captured (784 bits) on interface 0
Ethernet II, Src: Private_66:68:02 (00:50:79:66:68:02), Dst: Private_66:68:01 (00:50:79:66:68:01)
Internet Protocol Version 4, Src: 10.2.2.3, Dst: 10.2.2.2
Internet Control Message Protocol

No.     Time           Source                Destination           Protocol Length Info
     42 39.017401      10.2.2.2              10.2.2.3              ICMP     98     Echo (ping) reply    id=0x658c, seq=2/512, ttl=64 (request in 41)

Frame 42: 98 bytes on wire (784 bits), 98 bytes captured (784 bits) on interface 0
Ethernet II, Src: Private_66:68:01 (00:50:79:66:68:01), Dst: Private_66:68:02 (00:50:79:66:68:02)
Internet Protocol Version 4, Src: 10.2.2.2, Dst: 10.2.2.3
Internet Control Message Protocol

No.     Time           Source                Destination           Protocol Length Info
     43 40.018960      10.2.2.3              10.2.2.2              ICMP     98     Echo (ping) request  id=0x668c, seq=3/768, ttl=64 (reply in 44)

Frame 43: 98 bytes on wire (784 bits), 98 bytes captured (784 bits) on interface 0
Ethernet II, Src: Private_66:68:02 (00:50:79:66:68:02), Dst: Private_66:68:01 (00:50:79:66:68:01)
Internet Protocol Version 4, Src: 10.2.2.3, Dst: 10.2.2.2
Internet Control Message Protocol

No.     Time           Source                Destination           Protocol Length Info
     44 40.019434      10.2.2.2              10.2.2.3              ICMP     98     Echo (ping) reply    id=0x668c, seq=3/768, ttl=64 (request in 43)

Frame 44: 98 bytes on wire (784 bits), 98 bytes captured (784 bits) on interface 0
Ethernet II, Src: Private_66:68:01 (00:50:79:66:68:01), Dst: Private_66:68:02 (00:50:79:66:68:02)
Internet Protocol Version 4, Src: 10.2.2.2, Dst: 10.2.2.3
Internet Control Message Protocol

No.     Time           Source                Destination           Protocol Length Info
     45 40.146251      aa:bb:cc:00:01:10     Spanning-tree-(for-bridges)_00 STP      60     RST. Root = 32768/1/aa:bb:cc:00:01:00  Cost = 0  Port = 0x8002

Frame 45: 60 bytes on wire (480 bits), 60 bytes captured (480 bits) on interface 0
IEEE 802.3 Ethernet
Logical-Link Control
Spanning Tree Protocol

No.     Time           Source                Destination           Protocol Length Info
     46 40.641421      aa:bb:cc:00:01:10     CDP/VTP/DTP/PAgP/UDLD DTP      60     Dynamic Trunk Protocol

Frame 46: 60 bytes on wire (480 bits), 60 bytes captured (480 bits) on interface 0
IEEE 802.3 Ethernet
Logical-Link Control
Dynamic Trunk Protocol:  (Operating/Administrative): Access/Auto (0x04) (Operating/Administrative): ISL/Negotiated (0x40): aa:bb:cc:00:01:10

No.     Time           Source                Destination           Protocol Length Info
     47 40.641435      aa:bb:cc:00:01:10     CDP/VTP/DTP/PAgP/UDLD DTP      90     Dynamic Trunk Protocol

Frame 47: 90 bytes on wire (720 bits), 90 bytes captured (720 bits) on interface 0
ISL
IEEE 802.3 Ethernet
Logical-Link Control
Dynamic Trunk Protocol:  (Operating/Administrative): Access/Auto (0x04) (Operating/Administrative): ISL/Negotiated (0x40): aa:bb:cc:00:01:10

No.     Time           Source                Destination           Protocol Length Info
     48 40.642779      aa:bb:cc:00:03:10     CDP/VTP/DTP/PAgP/UDLD DTP      60     Dynamic Trunk Protocol

Frame 48: 60 bytes on wire (480 bits), 60 bytes captured (480 bits) on interface 0
IEEE 802.3 Ethernet
Logical-Link Control
Dynamic Trunk Protocol:  (Operating/Administrative): Access/Auto (0x04) (Operating/Administrative): ISL/Negotiated (0x40): aa:bb:cc:00:03:10

No.     Time           Source                Destination           Protocol Length Info
     49 40.642794      aa:bb:cc:00:03:10     CDP/VTP/DTP/PAgP/UDLD DTP      90     Dynamic Trunk Protocol

Frame 49: 90 bytes on wire (720 bits), 90 bytes captured (720 bits) on interface 0
ISL
IEEE 802.3 Ethernet
Logical-Link Control
Dynamic Trunk Protocol:  (Operating/Administrative): Access/Auto (0x04) (Operating/Administrative): ISL/Negotiated (0x40): aa:bb:cc:00:03:10

No.     Time           Source                Destination           Protocol Length Info
     50 40.643841      aa:bb:cc:00:03:20     CDP/VTP/DTP/PAgP/UDLD DTP      90     Dynamic Trunk Protocol

Frame 50: 90 bytes on wire (720 bits), 90 bytes captured (720 bits) on interface 0
ISL
IEEE 802.3 Ethernet
Logical-Link Control
Dynamic Trunk Protocol:  (Operating/Administrative): Access/Auto (0x04) (Operating/Administrative): ISL/Negotiated (0x40): aa:bb:cc:00:03:20

No.     Time           Source                Destination           Protocol Length Info
     51 40.665139      aa:bb:cc:00:02:00     CDP/VTP/DTP/PAgP/UDLD DTP      90     Dynamic Trunk Protocol

Frame 51: 90 bytes on wire (720 bits), 90 bytes captured (720 bits) on interface 0
ISL
IEEE 802.3 Ethernet
Logical-Link Control
Dynamic Trunk Protocol:  (Operating/Administrative): Access/Auto (0x04) (Operating/Administrative): ISL/Negotiated (0x40): aa:bb:cc:00:02:00

No.     Time           Source                Destination           Protocol Length Info
     52 41.020708      10.2.2.3              10.2.2.2              ICMP     98     Echo (ping) request  id=0x678c, seq=4/1024, ttl=64 (reply in 53)

Frame 52: 98 bytes on wire (784 bits), 98 bytes captured (784 bits) on interface 0
Ethernet II, Src: Private_66:68:02 (00:50:79:66:68:02), Dst: Private_66:68:01 (00:50:79:66:68:01)
Internet Protocol Version 4, Src: 10.2.2.3, Dst: 10.2.2.2
Internet Control Message Protocol

No.     Time           Source                Destination           Protocol Length Info
     53 41.021350      10.2.2.2              10.2.2.3              ICMP     98     Echo (ping) reply    id=0x678c, seq=4/1024, ttl=64 (request in 52)

Frame 53: 98 bytes on wire (784 bits), 98 bytes captured (784 bits) on interface 0
Ethernet II, Src: Private_66:68:01 (00:50:79:66:68:01), Dst: Private_66:68:02 (00:50:79:66:68:02)
Internet Protocol Version 4, Src: 10.2.2.2, Dst: 10.2.2.3
Internet Control Message Protocol

No.     Time           Source                Destination           Protocol Length Info
     54 42.022549      10.2.2.3              10.2.2.2              ICMP     98     Echo (ping) request  id=0x688c, seq=5/1280, ttl=64 (reply in 55)

Frame 54: 98 bytes on wire (784 bits), 98 bytes captured (784 bits) on interface 0
Ethernet II, Src: Private_66:68:02 (00:50:79:66:68:02), Dst: Private_66:68:01 (00:50:79:66:68:01)
Internet Protocol Version 4, Src: 10.2.2.3, Dst: 10.2.2.2
Internet Control Message Protocol

No.     Time           Source                Destination           Protocol Length Info
     55 42.023051      10.2.2.2              10.2.2.3              ICMP     98     Echo (ping) reply    id=0x688c, seq=5/1280, ttl=64 (request in 54)

Frame 55: 98 bytes on wire (784 bits), 98 bytes captured (784 bits) on interface 0
Ethernet II, Src: Private_66:68:01 (00:50:79:66:68:01), Dst: Private_66:68:02 (00:50:79:66:68:02)
Internet Protocol Version 4, Src: 10.2.2.2, Dst: 10.2.2.3
Internet Control Message Protocol
```
</details>

<details>
<summary>IOU1 > IOU3</summary>

```bash 
No.     Time           Source                Destination           Protocol Length Info
      7 11.923385      Private_66:68:02      Broadcast             ARP      64     Who has 10.2.2.2? Tell 10.2.2.3

Frame 7: 64 bytes on wire (512 bits), 64 bytes captured (512 bits) on interface 0
Ethernet II, Src: Private_66:68:02 (00:50:79:66:68:02), Dst: Broadcast (ff:ff:ff:ff:ff:ff)
Address Resolution Protocol (request)

No.     Time           Source                Destination           Protocol Length Info
      8 11.923436      Private_66:68:02      Broadcast             ARP      64     Who has 10.2.2.2? Tell 10.2.2.3

Frame 8: 64 bytes on wire (512 bits), 64 bytes captured (512 bits) on interface 0
Ethernet II, Src: Private_66:68:02 (00:50:79:66:68:02), Dst: Broadcast (ff:ff:ff:ff:ff:ff)
Address Resolution Protocol (request)

No.     Time           Source                Destination           Protocol Length Info
      9 11.923472      Private_66:68:02      Broadcast             ARP      64     Who has 10.2.2.2? Tell 10.2.2.3

Frame 9: 64 bytes on wire (512 bits), 64 bytes captured (512 bits) on interface 0
Ethernet II, Src: Private_66:68:02 (00:50:79:66:68:02), Dst: Broadcast (ff:ff:ff:ff:ff:ff)
Address Resolution Protocol (request)

No.     Time           Source                Destination           Protocol Length Info
     10 11.923509      Private_66:68:02      Broadcast             ARP      64     Who has 10.2.2.2? Tell 10.2.2.3

Frame 10: 64 bytes on wire (512 bits), 64 bytes captured (512 bits) on interface 0
Ethernet II, Src: Private_66:68:02 (00:50:79:66:68:02), Dst: Broadcast (ff:ff:ff:ff:ff:ff)
Address Resolution Protocol (request)

No.     Time           Source                Destination           Protocol Length Info
     11 11.926828      Private_66:68:01      Private_66:68:02      ARP      64     10.2.2.2 is at 00:50:79:66:68:01

Frame 11: 64 bytes on wire (512 bits), 64 bytes captured (512 bits) on interface 0
Ethernet II, Src: Private_66:68:01 (00:50:79:66:68:01), Dst: Private_66:68:02 (00:50:79:66:68:02)
Address Resolution Protocol (reply)

No.     Time           Source                Destination           Protocol Length Info
     12 11.927260      Private_66:68:01      Private_66:68:02      ARP      64     10.2.2.2 is at 00:50:79:66:68:01

Frame 12: 64 bytes on wire (512 bits), 64 bytes captured (512 bits) on interface 0
Ethernet II, Src: Private_66:68:01 (00:50:79:66:68:01), Dst: Private_66:68:02 (00:50:79:66:68:02)
Address Resolution Protocol (reply)

No.     Time           Source                Destination           Protocol Length Info
     13 11.927613      Private_66:68:01      Private_66:68:02      ARP      64     10.2.2.2 is at 00:50:79:66:68:01

Frame 13: 64 bytes on wire (512 bits), 64 bytes captured (512 bits) on interface 0
Ethernet II, Src: Private_66:68:01 (00:50:79:66:68:01), Dst: Private_66:68:02 (00:50:79:66:68:02)
Address Resolution Protocol (reply)

No.     Time           Source                Destination           Protocol Length Info
     14 11.927874      Private_66:68:01      Private_66:68:02      ARP      64     10.2.2.2 is at 00:50:79:66:68:01

Frame 14: 64 bytes on wire (512 bits), 64 bytes captured (512 bits) on interface 0
Ethernet II, Src: Private_66:68:01 (00:50:79:66:68:01), Dst: Private_66:68:02 (00:50:79:66:68:02)
Address Resolution Protocol (reply)

No.     Time           Source                Destination           Protocol Length Info
     15 11.928134      Private_66:68:01      Private_66:68:02      ARP      64     10.2.2.2 is at 00:50:79:66:68:01

Frame 15: 64 bytes on wire (512 bits), 64 bytes captured (512 bits) on interface 0
Ethernet II, Src: Private_66:68:01 (00:50:79:66:68:01), Dst: Private_66:68:02 (00:50:79:66:68:02)
Address Resolution Protocol (reply)

No.     Time           Source                Destination           Protocol Length Info
     16 11.928415      Private_66:68:01      Private_66:68:02      ARP      64     10.2.2.2 is at 00:50:79:66:68:01

Frame 16: 64 bytes on wire (512 bits), 64 bytes captured (512 bits) on interface 0
Ethernet II, Src: Private_66:68:01 (00:50:79:66:68:01), Dst: Private_66:68:02 (00:50:79:66:68:02)
Address Resolution Protocol (reply)

No.     Time           Source                Destination           Protocol Length Info
     17 11.928679      Private_66:68:01      Private_66:68:02      ARP      64     10.2.2.2 is at 00:50:79:66:68:01

Frame 17: 64 bytes on wire (512 bits), 64 bytes captured (512 bits) on interface 0
Ethernet II, Src: Private_66:68:01 (00:50:79:66:68:01), Dst: Private_66:68:02 (00:50:79:66:68:02)
Address Resolution Protocol (reply)

No.     Time           Source                Destination           Protocol Length Info
     18 11.928994      Private_66:68:01      Private_66:68:02      ARP      64     10.2.2.2 is at 00:50:79:66:68:01

Frame 18: 64 bytes on wire (512 bits), 64 bytes captured (512 bits) on interface 0
Ethernet II, Src: Private_66:68:01 (00:50:79:66:68:01), Dst: Private_66:68:02 (00:50:79:66:68:02)
Address Resolution Protocol (reply)

No.     Time           Source                Destination           Protocol Length Info
     19 11.929686      10.2.2.3              10.2.2.2              ICMP     98     Echo (ping) request  id=0x648c, seq=1/256, ttl=64 (reply in 20)

Frame 19: 98 bytes on wire (784 bits), 98 bytes captured (784 bits) on interface 0
Ethernet II, Src: Private_66:68:02 (00:50:79:66:68:02), Dst: Private_66:68:01 (00:50:79:66:68:01)
Internet Protocol Version 4, Src: 10.2.2.3, Dst: 10.2.2.2
Internet Control Message Protocol

No.     Time           Source                Destination           Protocol Length Info
     20 11.929968      10.2.2.2              10.2.2.3              ICMP     98     Echo (ping) reply    id=0x648c, seq=1/256, ttl=64 (request in 19)

Frame 20: 98 bytes on wire (784 bits), 98 bytes captured (784 bits) on interface 0
Ethernet II, Src: Private_66:68:01 (00:50:79:66:68:01), Dst: Private_66:68:02 (00:50:79:66:68:02)
Internet Protocol Version 4, Src: 10.2.2.2, Dst: 10.2.2.3
Internet Control Message Protocol

No.     Time           Source                Destination           Protocol Length Info
     21 12.055466      aa:bb:cc:00:01:20     Spanning-tree-(for-bridges)_00 STP      60     RST. Root = 32768/1/aa:bb:cc:00:01:00  Cost = 0  Port = 0x8003

Frame 21: 60 bytes on wire (480 bits), 60 bytes captured (480 bits) on interface 0
IEEE 802.3 Ethernet 
Logical-Link Control
Spanning Tree Protocol

No.     Time           Source                Destination           Protocol Length Info
     22 12.930967      10.2.2.3              10.2.2.2              ICMP     98     Echo (ping) request  id=0x658c, seq=2/512, ttl=64 (reply in 23)

Frame 22: 98 bytes on wire (784 bits), 98 bytes captured (784 bits) on interface 0
Ethernet II, Src: Private_66:68:02 (00:50:79:66:68:02), Dst: Private_66:68:01 (00:50:79:66:68:01)
Internet Protocol Version 4, Src: 10.2.2.3, Dst: 10.2.2.2
Internet Control Message Protocol

No.     Time           Source                Destination           Protocol Length Info
     23 12.931272      10.2.2.2              10.2.2.3              ICMP     98     Echo (ping) reply    id=0x658c, seq=2/512, ttl=64 (request in 22)

Frame 23: 98 bytes on wire (784 bits), 98 bytes captured (784 bits) on interface 0
Ethernet II, Src: Private_66:68:01 (00:50:79:66:68:01), Dst: Private_66:68:02 (00:50:79:66:68:02)
Internet Protocol Version 4, Src: 10.2.2.2, Dst: 10.2.2.3
Internet Control Message Protocol

No.     Time           Source                Destination           Protocol Length Info
     24 13.932997      10.2.2.3              10.2.2.2              ICMP     98     Echo (ping) request  id=0x668c, seq=3/768, ttl=64 (reply in 25)

Frame 24: 98 bytes on wire (784 bits), 98 bytes captured (784 bits) on interface 0
Ethernet II, Src: Private_66:68:02 (00:50:79:66:68:02), Dst: Private_66:68:01 (00:50:79:66:68:01)
Internet Protocol Version 4, Src: 10.2.2.3, Dst: 10.2.2.2
Internet Control Message Protocol

No.     Time           Source                Destination           Protocol Length Info
     25 13.933300      10.2.2.2              10.2.2.3              ICMP     98     Echo (ping) reply    id=0x668c, seq=3/768, ttl=64 (request in 24)

Frame 25: 98 bytes on wire (784 bits), 98 bytes captured (784 bits) on interface 0
Ethernet II, Src: Private_66:68:01 (00:50:79:66:68:01), Dst: Private_66:68:02 (00:50:79:66:68:02)
Internet Protocol Version 4, Src: 10.2.2.2, Dst: 10.2.2.3
Internet Control Message Protocol

No.     Time           Source                Destination           Protocol Length Info
     26 14.060251      aa:bb:cc:00:01:20     Spanning-tree-(for-bridges)_00 STP      60     RST. Root = 32768/1/aa:bb:cc:00:01:00  Cost = 0  Port = 0x8003

Frame 26: 60 bytes on wire (480 bits), 60 bytes captured (480 bits) on interface 0
IEEE 802.3 Ethernet 
Logical-Link Control
Spanning Tree Protocol

No.     Time           Source                Destination           Protocol Length Info
     27 14.555293      aa:bb:cc:00:01:20     CDP/VTP/DTP/PAgP/UDLD DTP      60     Dynamic Trunk Protocol

Frame 27: 60 bytes on wire (480 bits), 60 bytes captured (480 bits) on interface 0
IEEE 802.3 Ethernet 
Logical-Link Control
Dynamic Trunk Protocol:  (Operating/Administrative): Access/Auto (0x04) (Operating/Administrative): ISL/Negotiated (0x40): aa:bb:cc:00:01:20

No.     Time           Source                Destination           Protocol Length Info
     28 14.555345      aa:bb:cc:00:01:20     CDP/VTP/DTP/PAgP/UDLD DTP      90     Dynamic Trunk Protocol

Frame 28: 90 bytes on wire (720 bits), 90 bytes captured (720 bits) on interface 0
ISL
IEEE 802.3 Ethernet 
Logical-Link Control
Dynamic Trunk Protocol:  (Operating/Administrative): Access/Auto (0x04) (Operating/Administrative): ISL/Negotiated (0x40): aa:bb:cc:00:01:20

No.     Time           Source                Destination           Protocol Length Info
     29 14.557008      aa:bb:cc:00:03:10     CDP/VTP/DTP/PAgP/UDLD DTP      90     Dynamic Trunk Protocol

Frame 29: 90 bytes on wire (720 bits), 90 bytes captured (720 bits) on interface 0
ISL
IEEE 802.3 Ethernet 
Logical-Link Control
Dynamic Trunk Protocol:  (Operating/Administrative): Access/Auto (0x04) (Operating/Administrative): ISL/Negotiated (0x40): aa:bb:cc:00:03:10

No.     Time           Source                Destination           Protocol Length Info
     30 14.557498      aa:bb:cc:00:03:20     CDP/VTP/DTP/PAgP/UDLD DTP      90     Dynamic Trunk Protocol

Frame 30: 90 bytes on wire (720 bits), 90 bytes captured (720 bits) on interface 0
ISL
IEEE 802.3 Ethernet 
Logical-Link Control
Dynamic Trunk Protocol:  (Operating/Administrative): Access/Auto (0x04) (Operating/Administrative): ISL/Negotiated (0x40): aa:bb:cc:00:03:20

No.     Time           Source                Destination           Protocol Length Info
     31 14.578333      aa:bb:cc:00:02:00     CDP/VTP/DTP/PAgP/UDLD DTP      60     Dynamic Trunk Protocol

Frame 31: 60 bytes on wire (480 bits), 60 bytes captured (480 bits) on interface 0
IEEE 802.3 Ethernet 
Logical-Link Control
Dynamic Trunk Protocol:  (Operating/Administrative): Access/Auto (0x04) (Operating/Administrative): ISL/Negotiated (0x40): aa:bb:cc:00:02:00

No.     Time           Source                Destination           Protocol Length Info
     32 14.578351      aa:bb:cc:00:02:00     CDP/VTP/DTP/PAgP/UDLD DTP      90     Dynamic Trunk Protocol

Frame 32: 90 bytes on wire (720 bits), 90 bytes captured (720 bits) on interface 0
ISL
IEEE 802.3 Ethernet 
Logical-Link Control
Dynamic Trunk Protocol:  (Operating/Administrative): Access/Auto (0x04) (Operating/Administrative): ISL/Negotiated (0x40): aa:bb:cc:00:02:00

No.     Time           Source                Destination           Protocol Length Info
     33 14.934746      10.2.2.3              10.2.2.2              ICMP     98     Echo (ping) request  id=0x678c, seq=4/1024, ttl=64 (reply in 34)

Frame 33: 98 bytes on wire (784 bits), 98 bytes captured (784 bits) on interface 0
Ethernet II, Src: Private_66:68:02 (00:50:79:66:68:02), Dst: Private_66:68:01 (00:50:79:66:68:01)
Internet Protocol Version 4, Src: 10.2.2.3, Dst: 10.2.2.2
Internet Control Message Protocol

No.     Time           Source                Destination           Protocol Length Info
     34 14.935176      10.2.2.2              10.2.2.3              ICMP     98     Echo (ping) reply    id=0x678c, seq=4/1024, ttl=64 (request in 33)

Frame 34: 98 bytes on wire (784 bits), 98 bytes captured (784 bits) on interface 0
Ethernet II, Src: Private_66:68:01 (00:50:79:66:68:01), Dst: Private_66:68:02 (00:50:79:66:68:02)
Internet Protocol Version 4, Src: 10.2.2.2, Dst: 10.2.2.3
Internet Control Message Protocol

No.     Time           Source                Destination           Protocol Length Info
     35 15.936650      10.2.2.3              10.2.2.2              ICMP     98     Echo (ping) request  id=0x688c, seq=5/1280, ttl=64 (reply in 36)

Frame 35: 98 bytes on wire (784 bits), 98 bytes captured (784 bits) on interface 0
Ethernet II, Src: Private_66:68:02 (00:50:79:66:68:02), Dst: Private_66:68:01 (00:50:79:66:68:01)
Internet Protocol Version 4, Src: 10.2.2.3, Dst: 10.2.2.2
Internet Control Message Protocol

No.     Time           Source                Destination           Protocol Length Info
     36 15.936917      10.2.2.2              10.2.2.3              ICMP     98     Echo (ping) reply    id=0x688c, seq=5/1280, ttl=64 (request in 35)

Frame 36: 98 bytes on wire (784 bits), 98 bytes captured (784 bits) on interface 0
Ethernet II, Src: Private_66:68:01 (00:50:79:66:68:01), Dst: Private_66:68:02 (00:50:79:66:68:02)
Internet Protocol Version 4, Src: 10.2.2.2, Dst: 10.2.2.3
Internet Control Message Protocol
```

</details>

<details>
<summary>IOU2 > IOU3</summary>

```bash
No.     Time           Source                Destination           Protocol Length Info
     14 23.965212      Private_66:68:02      Broadcast             ARP      64     Who has 10.2.2.2? Tell 10.2.2.3

Frame 14: 64 bytes on wire (512 bits), 64 bytes captured (512 bits) on interface 0
Ethernet II, Src: Private_66:68:02 (00:50:79:66:68:02), Dst: Broadcast (ff:ff:ff:ff:ff:ff)
Address Resolution Protocol (request)

No.     Time           Source                Destination           Protocol Length Info
     15 23.965285      Private_66:68:02      Broadcast             ARP      64     Who has 10.2.2.2? Tell 10.2.2.3

Frame 15: 64 bytes on wire (512 bits), 64 bytes captured (512 bits) on interface 0
Ethernet II, Src: Private_66:68:02 (00:50:79:66:68:02), Dst: Broadcast (ff:ff:ff:ff:ff:ff)
Address Resolution Protocol (request)

No.     Time           Source                Destination           Protocol Length Info
     16 23.965323      Private_66:68:02      Broadcast             ARP      64     Who has 10.2.2.2? Tell 10.2.2.3

Frame 16: 64 bytes on wire (512 bits), 64 bytes captured (512 bits) on interface 0
Ethernet II, Src: Private_66:68:02 (00:50:79:66:68:02), Dst: Broadcast (ff:ff:ff:ff:ff:ff)
Address Resolution Protocol (request)

No.     Time           Source                Destination           Protocol Length Info
     17 23.965358      Private_66:68:02      Broadcast             ARP      64     Who has 10.2.2.2? Tell 10.2.2.3

Frame 17: 64 bytes on wire (512 bits), 64 bytes captured (512 bits) on interface 0
Ethernet II, Src: Private_66:68:02 (00:50:79:66:68:02), Dst: Broadcast (ff:ff:ff:ff:ff:ff)
Address Resolution Protocol (request)

No.     Time           Source                Destination           Protocol Length Info
     18 23.965395      Private_66:68:02      Broadcast             ARP      64     Who has 10.2.2.2? Tell 10.2.2.3

Frame 18: 64 bytes on wire (512 bits), 64 bytes captured (512 bits) on interface 0
Ethernet II, Src: Private_66:68:02 (00:50:79:66:68:02), Dst: Broadcast (ff:ff:ff:ff:ff:ff)
Address Resolution Protocol (request)

No.     Time           Source                Destination           Protocol Length Info
     19 23.965454      Private_66:68:02      Broadcast             ARP      64     Who has 10.2.2.2? Tell 10.2.2.3

Frame 19: 64 bytes on wire (512 bits), 64 bytes captured (512 bits) on interface 0
Ethernet II, Src: Private_66:68:02 (00:50:79:66:68:02), Dst: Broadcast (ff:ff:ff:ff:ff:ff)
Address Resolution Protocol (request)

No.     Time           Source                Destination           Protocol Length Info
     20 23.965492      Private_66:68:02      Broadcast             ARP      64     Who has 10.2.2.2? Tell 10.2.2.3

Frame 20: 64 bytes on wire (512 bits), 64 bytes captured (512 bits) on interface 0
Ethernet II, Src: Private_66:68:02 (00:50:79:66:68:02), Dst: Broadcast (ff:ff:ff:ff:ff:ff)
Address Resolution Protocol (request)

No.     Time           Source                Destination           Protocol Length Info
     21 23.965529      Private_66:68:02      Broadcast             ARP      64     Who has 10.2.2.2? Tell 10.2.2.3

Frame 21: 64 bytes on wire (512 bits), 64 bytes captured (512 bits) on interface 0
Ethernet II, Src: Private_66:68:02 (00:50:79:66:68:02), Dst: Broadcast (ff:ff:ff:ff:ff:ff)
Address Resolution Protocol (request)
```
</details>

Donc le root bridge est bien le IOU1 et les ports désactivé sont les ports `0/1` du switch IOU2 et `0/2` du IOU 3.


Pour la reconfiguration nous changeons le *root bridge* qui était sur IOU1 pour le mettre sur le IOU3. 

```
Avant le changement :
IOU1#show spanning-tree
VLAN0001
  Spanning tree enabled protocol rstp
  Root ID    Priority    32769
             Address     aabb.cc00.0100
             This bridge is the root
             Hello Time   2 sec  Max Age 20 sec  Forward Delay 15 sec

  Bridge ID  Priority    32769  (priority 32768 sys-id-ext 1)
             Address     aabb.cc00.0100
             Hello Time   2 sec  Max Age 20 sec  Forward Delay 15 sec
             Aging Time  300 sec


Après le changement :
IOU3#conf t
IOU3(config)#spanning-tree vlan 2 priority 4096
IOU3#show spanning-tree
VLAN0001
  Spanning tree enabled protocol rstp
  Root ID    Priority    4097
             Address     aabb.cc00.0300
             This bridge is the root
             Hello Time   2 sec  Max Age 20 sec  Forward Delay 15 sec

  Bridge ID  Priority    4097   (priority 4096 sys-id-ext 1)
             Address     aabb.cc00.0300
             Hello Time   2 sec  Max Age 20 sec  Forward Delay 15 sec
             Aging Time  300 sec

IOU1#show spanning-tree 
VLAN0001
  Spanning tree enabled protocol rstp
  Root ID    Priority    4097
             Address     aabb.cc00.0300
             Cost        100
             Port        2 (Ethernet0/1)
             Hello Time   2 sec  Max Age 20 sec  Forward Delay 15 sec

  Bridge ID  Priority    32769  (priority 32768 sys-id-ext 1)
             Address     aabb.cc00.0100
             Hello Time   2 sec  Max Age 20 sec  Forward Delay 15 sec
             Aging Time  300 sec
```

# III. Isolation

### 1.Simple

#### Topologie

```
+-----+        +-------+        +-----+
| PC1 +--------+  SW1  +--------+ PC3 |
+-----+    0/0 +-------+ 0/2    +-----+
  10           0/1 |              20
                   |
                +--+--+
                | PC2 |
                +-----+
                  20
```

#### Plan d'adressage

Machine | Ip | VLAN
--- | --- | --- 
Pc 1 | 10.2.3.1/24 | 10
Pc 2 | 10.2.3.2/24 | 20
Pc 3 | 10.2.3.3/24 | 20

#### Do

Pour configurer les vlan il faut faire ces étapes sur le switch : 

```
IOU1#conf t
Enter configuration commands, one per line.  End with CNTL/Z.
IOU1(config)#vlan 10
IOU1(config-vlan)#interface et 0/0
IOU1(config-if)#switchport mode access
IOU1(config-if)#switchport access vlan 10
IOU1(config-if)#exit
IOU1(config)#vlan 20
IOU1(config-vlan)#interface et 0/1
IOU1(config-if)#switchport mode access
IOU1(config-if)#switchport access vlan 20
IOU1(config-if)#exit
IOU1(config)#vlan 20                  
IOU1(config-vlan)#interface et 0/2         
IOU1(config-if)#switchport mode access   
IOU1(config-if)#switchport access vlan 20
IOU1(config-if)#exit                     
IOU1(config)#exit
IOU1#show vlan

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Et0/3, Et1/1, Et1/2, Et1/3
                                                Et2/0, Et2/1, Et2/2, Et2/3
                                                Et3/0, Et3/1, Et3/2, Et3/3
10   client-1                         active    Et0/0
20   client-23                        active    Et0/1, Et0/2, Et1/0
1002 fddi-default                     act/unsup
1003 token-ring-default               act/unsup
1004 fddinet-default                  act/unsup
1005 trnet-default                    act/unsup
```

Une fois configuré, nous allons tenter de ping entre les machines pour voir si tout à bien fonctionné.

```bash
Pc1;
PC1> ping 10.2.3.3
host (10.2.3.3) not reachable

Pc2:
PC2> ping 10.2.3.3
84 bytes from 10.2.3.3 icmp_seq=1 ttl=64 time=0.326 ms
84 bytes from 10.2.3.3 icmp_seq=2 ttl=64 time=0.547 ms
84 bytes from 10.2.3.3 icmp_seq=3 ttl=64 time=0.862 ms
84 bytes from 10.2.3.3 icmp_seq=4 ttl=64 time=0.474 ms
84 bytes from 10.2.3.3 icmp_seq=5 ttl=64 time=0.510 ms

PC2> ping 10.2.3.1
host (10.2.3.1) not reachable

Pc3:
PC3> ping 10.2.3.2  
84 bytes from 10.2.3.2 icmp_seq=1 ttl=64 time=0.428 ms
84 bytes from 10.2.3.2 icmp_seq=2 ttl=64 time=0.503 ms
84 bytes from 10.2.3.2 icmp_seq=3 ttl=64 time=0.490 ms
84 bytes from 10.2.3.2 icmp_seq=4 ttl=64 time=0.394 ms
84 bytes from 10.2.3.2 icmp_seq=5 ttl=64 time=0.453 ms
```
Le Pc2 et le Pc3 ne peuvent joindre le Pc1, mais peuvent se `ping` entre eux. Le Pc1 lui ne peut joindre personne.

<details>
<summary>Et1/0</summary>

Comme tu as du voir je me suis trompé dans la conf du vlan j'ai aussi ajouté le Et1/0, j'ai donc cherché comment le supprimer autre qu'en le delete du compte rendu. 

```
IOU1#conf t   
IOU1(config)#vlan 20                   
IOU1(config-vlan)#interface et 1/0          
IOU1(config-if)#no switchport mode access 
IOU1(config-if)#no switchport access vlan 20
IOU1(config-if)#exit 
IOU1(config)#exit 
```
C'est inutile dans le cadre de ce Tp, mais en vrai c'est toujours bon de savoir comment on en enlève que juste savoir comment on les ajoutes.
</details>

### 2. Avec Trunk

#### Topologie

```
+-----+        +-------+        +-------+        +-----+
| PC1 +--------+  SW1  +--------+  SW2  +--------+ PC4 |
+-----+    0/0 +-------+        +-------+ 0/0    +-----+
  10           0/1 |            0/1 |              20
                   |                |
                +--+--+          +--+--+
                | PC2 |          | PC3 |
                +-----+          +-----+
                  20               10 

```

#### Plan d'adressage

Machine | Net1 | Net2 | Vlan | Port 
--- | --- | --- | --- | ---- 
Pc 1 | 10.2.10.1/24 | xXx | 10 | 0/0 
Pc 2 | xXx | 10.2.20.1/24 | 20 | 0/1 
Pc 3 | 10.2.10.2/24 | xXx  | 10 |0/1 
Pc 4 | xXx | 10.2.20.2/24 | 20 | 0/0 

#### Do

Pour la configuration des switch c'est de la même façon que juste au dessus. Cette fois le tout est relié par un lien trunk, il permet de faire transiter plusieurs Vlan, faut donc lui indiquer lesquel. 

```
Config :
IOU1(config)#interface et 0/2
IOU1(config-if)#switchport trunk encapsulation dot1q
IOU1(config-if)#switchport mode trunk
IOU1(config-if)#switchport trunk allowed vlan 10,20

Test Ping:
PC3> ping 10.2.10.1
84 bytes from 10.2.10.1 icmp_seq=1 ttl=64 time=0.519 ms
84 bytes from 10.2.10.1 icmp_seq=2 ttl=64 time=0.815 ms
84 bytes from 10.2.10.1 icmp_seq=3 ttl=64 time=0.629 ms
84 bytes from 10.2.10.1 icmp_seq=4 ttl=64 time=0.647 ms
84 bytes from 10.2.10.1 icmp_seq=5 ttl=64 time=0.623 ms

PC3> ping 10.2.20.2
No gateway found

PC4> ping 10.2.20.1
84 bytes from 10.2.20.1 icmp_seq=1 ttl=64 time=0.709 ms
84 bytes from 10.2.20.1 icmp_seq=2 ttl=64 time=0.771 ms
84 bytes from 10.2.20.1 icmp_seq=3 ttl=64 time=0.739 ms
84 bytes from 10.2.20.1 icmp_seq=4 ttl=64 time=0.779 ms
84 bytes from 10.2.20.1 icmp_seq=5 ttl=64 time=0.697 ms

PC4> ping 10.2.10.1
No gateway found
```
# IV. Need Perfs

### 1. Topologie

```
+-----+        +-------+--------+-------+        +-----+
| PC1 +--------+  SW1  |        |  SW2  +--------+ PC4 |
+-----+    0/0 +-------+--------+-------+ 0/0    +-----+
  10           0/1 |            0/1 |              20
                   |                |
                +--+--+          +--+--+
                | PC2 |          | PC3 |
                +-----+          +-----+
                  20               10

```

### 2. Plan d'adressage

Machine | Net1 | Net2 | Vlan | Port 
--- | --- | --- | --- | ---- 
Pc 1 | 10.2.10.1/24 | xXx | 10 | 0/0 
Pc 2 | xXx | 10.2.20.1/24 | 20 | 0/1 
Pc 3 | 10.2.10.2/24 | xXx | 10 |0/1 
Pc 4 | xXx | 10.2.20.2/24 | 20 | 0/0 

### 3. Do


