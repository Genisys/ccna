Tp-Léo

# B2 Réseau 2019 - TP4 : Buffet à volonté

## Sommaire

* [Sujet global](#sujet-global)
    * [La Topologie](#la-topologie)
        * [Schéma GNS](#schéma-gns)
        * [Tableau des réseaux](#tableau-des-réseaux)
        * [Tableau d'adressage](#tableau-dadressage)
        * [Fichier de Conf](#fichier-de-conf)
            * [Conf Routeur](#conf-routeur)
            * [Conf Swicth](#conf-switch)
            * [Conf Vpcs](#conf-vpcs)
            * [Conf DHCP](#conf-dhcp)
            * [Conf DNS](#conf-dns)
            * [Conf serveur web](#conf-serveur-web)
        * [Checklist](#checklist)
* [Sujet au choix](#sujet-au-choix)
    * [Métrologie Réseau : SNMP, monitoring, gestion de logs](#métrologie-réseau-snmp-monitoring-gestion-de-logs)
        * [Préparation](#preparation)
        * [Étape Final](#étape-final)

# Sujet global

## La Topologie

### Schéma GNS

<div align="center"><img src="./img/schema.png" /></div>

### Tableau des réseaux

| Name     | Address        | VLAN |
|----------|----------------|------|
| `Admin` | `10.5.10.0/24` | 10   |
| `Client` | `10.5.20.0/24` | 20   |
| `Infra`  | `10.5.30.0/24` | 30   |

### Tableau d'adressage

| Machine  | `admins`      | `guests`      | `infra`       |
|----------|---------------|---------------|---------------|
| `r1`     | `10.5.10.254` | `10.5.20.254` | `10.5.30.254` |
| `admin1` | `10.5.10.11`  | x             | x             |
| `admin2` | `10.5.10.12`  | x             | x             |
| `admin3` | `10.5.10.13`  | x             | x             |
| `user1` | x             | `10.5.20.11`  | x             |
| `user2` | x             | `10.5.20.12`  | x             |
| `user3` | x             | `10.5.20.13`  | x             |
| `dhcp`   | x             | `10.5.20.253` | x             |
| `dns`    | x             | x             | `10.5.30.11`  |
| `web`    | x             | x             | `10.5.30.12`  |
| `snmp`   | x             | x             | `10.5.30.14`  |

### Fichier de Conf

#### Conf routeur :

<details>
<summary>Routeur 1</summary>

```cisco
!         
interface FastEthernet0/0
 ip address dhcp
 ip nat outside
 ip virtual-reassembly in
 duplex half
!         
interface FastEthernet1/0
 no ip address
 duplex auto
 speed auto
!         
interface FastEthernet1/0.30
 encapsulation dot1Q 30
 ip address 10.5.30.254 255.255.255.0
 ip nat inside
 ip virtual-reassembly in
!         
interface FastEthernet1/1
 no ip address
 duplex auto
 speed auto
!         
interface FastEthernet1/1.10
 encapsulation dot1Q 10
 ip address 10.5.10.254 255.255.255.0
 ip nat inside
 ip virtual-reassembly in
!         
interface FastEthernet1/1.20
 encapsulation dot1Q 20
 ip address 10.5.20.254 255.255.255.0
 ip nat inside
 ip virtual-reassembly in
!         
```

</details>

####  Cont Switch

<details>
<summary>Infra</summary>

```cisco
!
interface Ethernet0/0
 switchport trunk allowed vlan 30
 switchport trunk encapsulation dot1q
 switchport mode trunk
!         
interface Ethernet0/1
 switchport access vlan 30
 switchport mode access
!         
interface Ethernet0/2
 switchport access vlan 30
 switchport mode access
!         
interface Ethernet0/3
 switchport access vlan 30
 switchport mode access
!       
```

</details>

<details>
<summary>Client1</summary>

```cisco
!         
interface Ethernet0/0
 switchport trunk allowed vlan 10,20
 switchport trunk encapsulation dot1q
 switchport mode trunk
!         
interface Ethernet0/1
 switchport access vlan 10
 switchport mode access
!         
interface Ethernet0/2
 switchport access vlan 20
 switchport mode access
!   
```

</details>

<details>
<summary>Client2</summary>

```cisco
!
interface Ethernet0/0
 switchport trunk allowed vlan 10,20
 switchport trunk encapsulation dot1q
 switchport mode trunk
!         
interface Ethernet0/1
 switchport trunk allowed vlan 10,20
 switchport trunk encapsulation dot1q
 switchport mode trunk
!         
interface Ethernet0/2
!         
interface Ethernet0/3
 switchport trunk allowed vlan 10,20
 switchport trunk encapsulation dot1q
 switchport mode trunk
!         
interface Ethernet1/0
!         
interface Ethernet1/1
 switchport access vlan 10
 switchport mode access
!         
interface Ethernet1/2
 switchport access vlan 20
 switchport mode access
!
```

</details>

<details>
<summary>Client3</summary>

```cisco
!         
interface Ethernet0/0
 switchport trunk allowed vlan 10,20
 switchport trunk encapsulation dot1q
 switchport mode trunk
!         
interface Ethernet0/1
 switchport access vlan 10
 switchport mode access
!         
interface Ethernet0/2
 switchport access vlan 20
 switchport mode access
!         
interface Ethernet0/3
 switchport access vlan 20
 switchport mode access
!         
```

</details>

#### Conf Vpcs

<details>
<summary>User</summary>

```cisco
User1> ip 10.5.20.11/24 10.5.20.254 
Checking for duplicate address...
PC1 : 10.5.20.11 255.255.255.0 gateway 10.5.20.254

User1> show ip

NAME        : User1[1]
IP/MASK     : 10.5.20.11/24
GATEWAY     : 10.5.20.254
DNS         : 
MAC         : 00:50:79:66:68:05
LPORT       : 10043
RHOST:PORT  : 127.0.0.1:10044
MTU         : 1500

User2> ip 10.5.20.12/24 10.5.20.254 
Checking for duplicate address...
PC1 : 10.5.20.12 255.255.255.0 gateway 10.5.20.254

User2> show ip

NAME        : User2[1]
IP/MASK     : 10.5.20.12/24
GATEWAY     : 10.5.20.254
DNS         : 
MAC         : 00:50:79:66:68:02
LPORT       : 10037
RHOST:PORT  : 127.0.0.1:10038
MTU         : 1500

User3> ip 10.5.20.13/24 10.5.20.254
Checking for duplicate address...
PC1 : 10.5.20.13 255.255.255.0 gateway 10.5.20.254

User3> show ip

NAME        : User3[1]
IP/MASK     : 10.5.20.13/24
GATEWAY     : 10.5.20.254
DNS         : 
MAC         : 00:50:79:66:68:06
LPORT       : 10045
RHOST:PORT  : 127.0.0.1:10046
MTU         : 1500
```

</details>


<details>
<summary>Admin</summary>

```cisco
Admin1> ip 10.5.10.11/24 10.5.10.254
Checking for duplicate address...
PC1 : 10.5.10.11 255.255.255.0 gateway 10.5.10.254

Admin1> show ip

NAME        : Admin1[1]
IP/MASK     : 10.5.10.11/24
GATEWAY     : 10.5.10.254
DNS         : 
MAC         : 00:50:79:66:68:07
LPORT       : 10047
RHOST:PORT  : 127.0.0.1:10048
MTU         : 1500

Admin2> ip 10.5.10.12/24 10.5.10.254
Checking for duplicate address...
PC1 : 10.5.10.12 255.255.255.0 gateway 10.5.10.254

Admin2> show ip 

NAME        : Admin2[1]
IP/MASK     : 10.5.10.12/24
GATEWAY     : 10.5.10.254
DNS         : 
MAC         : 00:50:79:66:68:03
LPORT       : 10039
RHOST:PORT  : 127.0.0.1:10040
MTU         : 1500

Admin3> ip 10.5.10.13/24 10.5.10.254
Checking for duplicate address...
PC1 : 10.5.10.13 255.255.255.0 gateway 10.5.10.254

Admin3> show ip

NAME        : Admin3[1]
IP/MASK     : 10.5.10.13/24
GATEWAY     : 10.5.10.254
DNS         : 
MAC         : 00:50:79:66:68:04
LPORT       : 10041
RHOST:PORT  : 127.0.0.1:10042
MTU         : 1500

Dns> ip 10.5.30.11/24 10.5.30.254
Checking for duplicate address...
PC1 : 10.5.30.11 255.255.255.0 gateway 10.5.30.254

```

</details>

#### Conf DHCP

<details>
<summary>`dhcp.conf`</summary>

```bash
[idk@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf
default-lease-time 600;
max-lease-time 7200;

authoritative;

log-facility local7;

subnet 10.5.20.0 netmask 255.255.255.0 {

    option routers 10.5.20.254;
    option subnet-mask 255.255.255.0;
    option broadcast-address 10.5.20.255;

    # Pour Définir la range d'ip de nos clients
    range 10.5.20.75 10.5.20.100;

    option domain-name "tp5.b2";
    option domain-name-servers 10.5.20.11;
}
```

</details>

#### Conf DNS

<details>
<summary>`named.conf`</summary>

```bash
[idk@dns ~]$ sudo cat /etc/named.conf
options {
	listen-on port 53 { 127.0.0.1; 10.5.30.11;};
	listen-on-v6 port 53 { ::1; };
	directory 	"/var/named";
	dump-file 	"/var/named/data/cache_dump.db";
	statistics-file "/var/named/data/named_stats.txt";
	memstatistics-file "/var/named/data/named_mem_stats.txt";
	recursing-file  "/var/named/data/named.recursing";
	secroots-file   "/var/named/data/named.secroots";
	allow-query     { 10.5.20.0/24; 10.5.30.0/24; };
	recursion yes;

	dnssec-enable yes;
	dnssec-validation yes;

	/* Path to ISC DLV key */
	bindkeys-file "/etc/named.root.key";

	managed-keys-directory "/var/named/dynamic";

	pid-file "/run/named/named.pid";
	session-keyfile "/run/named/session.key";
};

logging {
        channel default_debug {
                file "data/named.run";
                severity dynamic;
        };
};

zone "." IN {
	type hint;
	file "named.ca";
};

zone "tp5.b2" IN {
	type master;
	file "/var/named/tp5.b2.db";
	allow-update { none; };
};

zone "20.5.10.in-addr.arpa" IN {
	type master;
	file "/var/named/20.5.10.db";
	allow-update { none; };
};

include "/etc/named.rfc1912.zones";
include "/etc/named.root.key";
```

</details>

<details>
<summary>`tp4.b2.db`</summary>

```{bash}
[idk@dns ~]$ sudo cat /var/named/tp5.b2.db
$TTL    604800
@   IN  SOA     ns1.tp5.b2. root.tp5.b2. (
                                                1010    ;Serial
                                                3H      ;Refresh
                                                15M     ;Retry
                                                1W      ;Expire
                                                1D      ;Minimum TTL
                                                )

;Name Server Information
@      IN  NS      ns1.tp5.b2.

ns1 IN  A       10.5.30.11
user1     IN  A       10.5.20.11
user2     IN  A       10.5.20.12
user3     IN  A       10.5.20.13
dhcp       IN  A       10.5.20.253

```

</details>

<details>
<summary>`20.5.10.db`</summary>

```{bash}
[idk@dns ~]$ sudo cat /var/named/20.5.10.db            
$TTL    604800
@   IN  SOA     ns1.tp5.b2. root.tp5.b2. (
                                                1010    ;Serial
                                                3H      ;Refresh
                                                15M     ;Retry
                                                1W      ;Expire
                                                1D      ;Minimum TTL
                                                )

;Name Server Information
@ IN  NS      ns1.tp5.b2.


11.30.5.10 IN PTR ns1.tp5.b2.

;PTR Record IP address to HostName
11      IN  PTR     user1.tp5.b2.
12      IN  PTR     user2.tp5.b2.
13      IN  PTR     user3.tp5.b2.
253      IN  PTR     dhcp.tp5.b2.
```

</details>

Une petite vérification s'impose au niveau des contextes de fichier histoire de pas voir si oui on non nous allons nous faire emmerder par Selinux. Pour cela nous allons utiliser la commande `ls` avec l'option `-Z`.

```bash
[idk@dns ~]$ sudo ls -Z /var/named/
-rw-r-----. root  named system_u:object_r:named_zone_t:s0 20.5.10.db
drwxrwx---. named named system_u:object_r:named_cache_t:s0 data
drwxrwx---. named named system_u:object_r:named_cache_t:s0 dynamic
-rw-r-----. root  named system_u:object_r:named_conf_t:s0 named.ca
-rw-r-----. root  named system_u:object_r:named_zone_t:s0 named.empty
-rw-r-----. root  named system_u:object_r:named_zone_t:s0 named.localhost
-rw-r-----. root  named system_u:object_r:named_zone_t:s0 named.loopback
drwxrwx---. named named system_u:object_r:named_cache_t:s0 slaves
-rw-r-----. root  named system_u:object_r:named_zone_t:s0 tp5.b2.db
```

Si un fichier à cette tête :
```
-rw-r--r--. root  root  unconfined_u:object_r:named_zone_t:s0 <ton ou tes fichiers>
```
Go faire cette commande voyou :

```bash
[idk@dns ~]$ sudo chcon -u system_u -r object_r -t named_zone_t <ton ou tes fichiers>
```
Pour les droits un petit `chown` suivi d'un `chmod` histoire de rendre tout ça clean :

```bash
[idk@dns ~]$ sudo chown root:named <ton ou tes fichiers>
[idk@dns ~]$ sudo chmod 640 <ton ou tes fichiers> 
```

#### Conf serveur web

Pour la configuration du serveur web il faut simplement suivre ces étapes, nous avons pris `nginx` pour la demomais c'est aussi possible avec `apache2` :

```bash
[idk@dns ~]$ sudo yum install epel-release -y
[idk@dns ~]$ sudo yum install nginx -y
[idk@dns ~]$ sudo systemctl enable nginx
[idk@dns ~]$ sudo systemctl start nginx
[idk@dns ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
[idk@dns ~]$ sudo firewall-cmd --reload
[idk@dns ~]$ sudo firewall-cmd --list-all # Pour vérifier 
[idk@dns ~]$ curl localhost # Également pour vérifier 
```
### Checklist

* Ping Admin

```
Admin1> ping 10.5.10.12

84 bytes from 10.5.10.12 icmp_seq=1 ttl=64 time=1.438 ms
84 bytes from 10.5.10.12 icmp_seq=2 ttl=64 time=1.134 ms
84 bytes from 10.5.10.12 icmp_seq=3 ttl=64 time=1.640 ms
84 bytes from 10.5.10.12 icmp_seq=4 ttl=64 time=1.230 ms
84 bytes from 10.5.10.12 icmp_seq=5 ttl=64 time=1.040 ms

Admin1> ping 10.5.10.13

84 bytes from 10.5.10.13 icmp_seq=1 ttl=64 time=0.964 ms
84 bytes from 10.5.10.13 icmp_seq=2 ttl=64 time=1.121 ms
84 bytes from 10.5.10.13 icmp_seq=3 ttl=64 time=1.195 ms
84 bytes from 10.5.10.13 icmp_seq=4 ttl=64 time=1.320 ms
84 bytes from 10.5.10.13 icmp_seq=5 ttl=64 time=1.139 ms
```

* Ping User

```
User1> ping 10.5.20.12

84 bytes from 10.5.20.12 icmp_seq=1 ttl=64 time=0.811 ms
84 bytes from 10.5.20.12 icmp_seq=2 ttl=64 time=1.040 ms
84 bytes from 10.5.20.12 icmp_seq=3 ttl=64 time=1.156 ms
84 bytes from 10.5.20.12 icmp_seq=4 ttl=64 time=1.243 ms
84 bytes from 10.5.20.12 icmp_seq=5 ttl=64 time=1.333 ms

User1> ping 10.5.20.13

84 bytes from 10.5.20.13 icmp_seq=1 ttl=64 time=0.835 ms
84 bytes from 10.5.20.13 icmp_seq=2 ttl=64 time=1.274 ms
84 bytes from 10.5.20.13 icmp_seq=3 ttl=64 time=1.320 ms
84 bytes from 10.5.20.13 icmp_seq=4 ttl=64 time=1.361 ms
84 bytes from 10.5.20.13 icmp_seq=5 ttl=64 time=1.207 ms
```

* Requête Site Web
```
[idk@dhcp ~]$ curl 10.5.30.12 |grep "<h1>" 
  <h1>Welcome to CentOS</h1>
```
# Sujet au choix

## Métrologie Réseau : SNMP, monitoring, gestion de logs

### Préparation

Nous allons ajouter les repos et installé les paquets nécéssaires

```{bash}
hostnamectl set-hostname snmp
[idk@snmp ~]$ sudo yum upgrade -y
[idk@snmp ~]$ sudo yum install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm \
http://yum.opennms.org/repofiles/opennms-repo-stable-rhel7.noarch.rpm \
http://rpms.remirepo.net/enterprise/remi-release-7.rpm \
-y
[idk@snmp ~]$ sudo yum install vim yum-utils -y
[idk@snmp ~]$ sudo yum-config-manager --enable remi-php72
[idk@snmp ~]$ sudo yum install wget.x86_64 httpd.x86_64 php72.x86_64 php72-php-opcache.x86_64 libvirt.x86_64 php72-php-mysql.x86_64 php72-php-gd.x86_64 php72-php-posix php72-php-pear.noarch cronie.x86_64 net-snmp.x86_64 net-snmp-utils.x86_64 fping.x86_64 mariadb-server.x86_64 mariadb.x86_64 MySQL-python.x86_64 rrdtool.x86_64 subversion.x86_64  jwhois.x86_64 ipmitool.x86_64 graphviz.x86_64 ImageMagick.x86_64 php72-php-sodium.x86_64 -y
```

Ensuite il nous faut télécharger `Observium`

```bash
[idk@snmp ~]$ sudo mkdir -p /opt/observium && cd /opt ; \
sudo wget http://www.observium.org/observium-community-latest.tar.gz ; \
sudo tar zxvf observium-community-latest.tar.gz
```

Pour ensuite faire la création et configuration de la base de données :

```bash
[idk@snmp opt]$ sudo systemctl enable mariadb ; \
sudo systemctl start mariadb
[idk@snmp opt]$ sudo mysql \
--user=root \
--password='rootoor' \
--execute="CREATE DATABASE observium DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;"

[idk@snmp opt]$ sudo mysql \
--user=root \
--password='rootoor' \
--database="observium" \
--execute="GRANT ALL PRIVILEGES ON observium.* TO 'observium'@'localhost' IDENTIFIED BY 'rootoor';"

[idk@snmp opt]$ cd observium
[idk@snmp opt]$ sudo cp config.php.default config.php
[idk@snmp opt]$ sudo ./discovery.php -u
```
Ensuite pour la configuration du système il nous faut déterminer ou à était installé `fping` et le rajouter au fichier de config `config.php` dans le répertoire de observium.

```bash
which fping
# Il faut rajouter cette ligne 
$config['fping'] = "/usr/bin/fping";
```

Je créer le fichier `/etc/httpd/conf.d/obversium.conf` et j'y ajoute ces lignes :

```
<VirtualHost *>
   DocumentRoot /opt/observium/html/
   ServerName  observium.domain.com
   CustomLog /opt/observium/logs/access_log combined
   ErrorLog /opt/observium/logs/error_log
   <Directory "/opt/observium/html/">
     AllowOverride All
     Options FollowSymLinks MultiViews
     Require all granted
   </Directory>
</VirtualHost>
```
Encore une fois cette histoire de contexte avec SeLinux NON JE NE FERAIS PAS `setenforce 0`

```bash
[idk@snmp ~]$ sudo chcon -u system_u -r object_r -t httpd_config_t /etc/httpd/conf.d/observium.conf
```

Je creer les dossiers de logs pour apache et faire en sorte qu'il appartient au groupe et au user apache

```bash
[idk@snmp ~]$ sudo mkdir /opt/observium/logs
[idk@snmp ~]$ sudo chown apache:apache /opt/observium/logs
```
J'ajoute un utilisateur

```bash
[idk@snmp ~]$ sudo cd /opt/observium
[idk@snmp ~]$ sudo ./adduser.php idk root 10
```

Tout d'abord il nous faut activer le `snmp` sur notre routeur pour cela je vous conseille de regarde [ici](https://www.cisco.com/c/en/us/support/docs/ip/simple-network-management-protocol-snmp/7282-12.html).

```bash
snmp-server community public RO
```

Le `RO` désigne Read-Only pour la commu `public`.

Nous ajoutons notre première machine via observium.

```bash
[idk@snmp ~]$ sudo ./add_device.php 10.5.20.254 public v2c
```


```bash
[idk@snmp ~]$ sudo ./discovery.php -h all
[idk@snmp ~]$ sudo ./poller.php -h all
```

Pour faire ces commande de façon automatique nous allons creer un tache cron pour cela on ajoute dans le fichier `/etc/cron.d/observium` :

```bash
# Execute cette commande toute les 6 heures
33  */6   * * *   root    /opt/observium/discovery.php -h all >> /dev/null 2>&1

# Execute cette commande toute les 5 minutes
*/5 *     * * *   root    /opt/observium/discovery.php -h new >> /dev/null 2>&1

# Execute cette commande toute les 5 minutes
*/5 *     * * *   root    /opt/observium/poller-wrapper.py >> /dev/null 2>&1

# Execute cette commande toute les jours
13 5 * * * root /opt/observium/housekeeping.php -ysel

# Execute cette commande toute les jours
47 4 * * * root /opt/observium/housekeeping.php -yrptb
```

Nous faisons un petit reload de la conf de `Crond`.

```bash
[idk@snmp observium]$ sudo systemctl reload crond   
```

Puis on `enable` le service de apache `httpd` sous Centos et on le start pour avoir l'interface web.

```bash
[idk@snmp observium]$ sudo systemctl enable httpd
[idk@snmp observium]$ sudo systemctl start httpd
```

Il ne faut pas oublier d'ouvrir le port 80 du firewall comme vu précédement.

```bash
[idk@dns ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
[idk@dns ~]$ sudo firewall-cmd --reload
[idk@dns ~]$ sudo firewall-cmd --list-all # Pour vérifier
```

### Étape Final

Nous allons maintenant faire la configuration sur les clients, sur ma vm Web et Dhcp ainsi que Dns.

```bash
[idk@{dns,dhcp,web} ~]$ sudo sudo yum install -y telnet net-snmp net-snmp-utils ; \
sudo cp /etc/snmp/snmpd.conf /etc/snmp/snmpd.conf.old ; \
sudo firewall-cmd --add-port=161/udp --permanent ; \
sudo firewall-cmd --reload
```
