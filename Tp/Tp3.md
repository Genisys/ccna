Tp-Léo

# B2 Réseau 2019 - TP3: Routage INTER-VLAN + Mise en situation

## Sommaire

* [I. Router-on-a-Stick](#i-router-on-a-stick)
* [II. Cas Concret](#ii-cas-concret)

# I. Router-on-a-Stick

Petit résumé de la configuration demandé.

* Topologie


```
             +--+
             |R1|
             +-++
               |
           1/0 |             0/1      +---+
               |            +---------+PC4|
+---+    0/1 +-+-+ <-0/0-> +---+      +---+
|PC1+--------+SW1+---------+SW2|
+---+        +-+-+         +-+--+
           0/2 |         0/2 |  | 0/3
               |             |  +------+--+
               |             |         |P1|
             +-+-+         +-+-+       +--+
             |PC2|         |PC3|
             +---+         +---+
```

* Plan d'adressage

Machine | Ip `10`| Ip `20` | Ip `30` |  Ip `40` | Vlan
--- | --- | --- | --- | --- | --- 
PC1 | `10.3.10.1/24` | xXx | xXx | xXx | 10
PC2 | xXx | `10.3.20.2/24` | xXx | xXx | xXx | 20
PC3 | xXx | `10.3.20.3/24` | xXx | xXx | xXx | 20
PC4 | xXx | xXx |  `10.3.30.4/24` | xXx | xXx | 30
P1 | xXx | xXx | xXx | `10.3.40.1/24` | 40
R1 | `10.3.10.254/24` | `10.3.20.254/24` | `10.3.30.254/24` | `10.3.40.254/24` | xXx

Maintenant il que toute la partie théorique est passé nous passons à la pratique.

* Configuration des Clients

Nous allons faire cette commande sur nos 4 machines mais avec le réseau et leur ip définie dans notre tableau d'addressage.

Réseau | Adresse | Vlan
--- | --- | --- 
10 | `10.3.10.0/24` | Client10 
20 | `10.3.20.0/24` | Client20 
30 | `10.3.30.0/24` | Client30 
40 | `10.3.40.0/24` | Periph (40) 

```bash
PC4> ip 10.3.30.4/24 10.3.30.254
```

Pour vérifier si tout à bien fonctionné :
```bash
PC4> show ip

NAME        : PC4[1]
IP/MASK     : 10.3.30.4/24
GATEWAY     : 10.3.30.254
DNS         :
MAC         : 00:50:79:66:68:03
LPORT       : 10022
RHOST:PORT  : 127.0.0.1:10023
MTU:        : 1500
```

<details>
<summary>Configuration des machines entière</summary>

Pc 1 :
```bash
PC1> ip 10.3.10.1/24 10.3.10.254
Checking for duplicate address...
PC1 : 10.3.10.1 255.255.255.0 gateway 10.3.10.254

PC1> show ip

NAME        : PC1[1]
IP/MASK     : 10.3.10.1/24
GATEWAY     : 10.3.10.254
DNS         :
MAC         : 00:50:79:66:68:00
LPORT       : 10016
RHOST:PORT  : 127.0.0.1:10017
MTU:        : 1500
```

Pc 2 :
```bash
PC2> ip 10.3.20.2/24 10.3.20.254
Checking for duplicate address...
PC1 : 10.3.20.2 255.255.255.0 gateway 10.3.20.254

PC2> show ip

NAME        : PC2[1]
IP/MASK     : 10.3.20.2/24
GATEWAY     : 10.3.20.254
DNS         :
MAC         : 00:50:79:66:68:01
LPORT       : 10018
RHOST:PORT  : 127.0.0.1:10019
MTU:        : 1500
```
Pc 3 :
```bash
PC3> ip 10.3.20.3/24 10.3.20.254
Checking for duplicate address...
PC1 : 10.3.20.3 255.255.255.0 gateway 10.3.20.254

PC3> show ip

NAME        : PC3[1]
IP/MASK     : 10.3.20.3/24
GATEWAY     : 10.3.20.254
DNS         :
MAC         : 00:50:79:66:68:02
LPORT       : 10020
RHOST:PORT  : 127.0.0.1:10021
MTU:        : 1500
```

</details>

Okay maintenant que l'on est dans le bon mood on se lance dans la configuration des routeurs et switch. 

* Switch

Nous allons d'abord définir un nom à nos Vlan sur les **deux** switch :

```cisco
IOU(1,2)(config)#vlan 10
IOU(1,2)(config-vlan)#name Client10
IOU(1,2)(config-vlan)#exit 
IOU(1,2)(config)#vlan 20
IOU(1,2)(config-vlan)#name Client20
IOU(1,2)(config-vlan)#exit
IOU(1,2)(config)#vlan 30
IOU(1,2)(config-vlan)#name Client30
IOU(1,2)(config-vlan)#exit
IOU(1,2)(config)#vlan 40
IOU(1,2)(config-vlan)#name Periph
IOU(1,2)(config-vlan)#exit
```

Ensuite nous faisons les interfaces avec l'accés pour les Vlan.

```cisco
Switch 1 :
SW1(config)#interface ethernet 0/0
SW1(config-if)#switchport trunk encapsulation dot1q
SW1(config-if)#switchport mode trunk
SW1(config-if)#switchport trunk allowed vlan 20,30,40
SW1(config-if)#exit
SW1(config)#interface ethernet 0/1
SW1(config-if)#switchport mode access
SW1(config-if)#switchport access vlan 10
SW1(config-if)#exit
SW1(config)#interface ethernet 0/2
SW1(config-if)#switchport mode access
SW1(config-if)#switchport access vlan 20
SW1(config-if)#exit
SW1(config)#interface ethernet 1/0
SW1(config-if)#switchport trunk encapsulation dot1q
SW1(config-if)#switchport mode trunk
SW1(config-if)#switchport trunk allowed vlan 20,30,40
SW1(config-if)#exit

Switch 2 :
SW2(config)#interface ethernet 0/0
SW2(config-if)#switchport trunk encapsulation dot1q
SW2(config-if)#switchport mode trunk
SW2(config-if)#switchport trunk allowed vlan 20,30,40
SW2(config-if)#exit
SW2(config)#exit
SW2(config)#interface ethernet 0/2
SW2(config-if)#switchport mode access
SW2(config-if)#switchport access vlan 20
SW2(config-if)#exit
SW2(config)#interface ethernet 0/1
SW2(config-if)#switchport mode access
SW2(config-if)#switchport access vlan 30
SW2(config-if)#exit
SW2(config)#interface ethernet 0/3
SW2(config-if)#switchport mode access
SW2(config-if)#switchport access vlan 40
SW2(config-if)#exit
```

Pour vérifier si tout les trunks ont bien était prit en compte la commande `show interfaces trunk` peut aider, après on peut aussi prendre une seule interface en l'indiquant `show interfaces ethernet 1/0 trunk`.

<details>
<summary>Détails</summary>

```cisco
IOU1#show interfaces trunk

Port        Mode             Encapsulation  Status        Native vlan
Et0/0       on               802.1q         trunking      1
Et1/0       on               802.1q         trunking      1

Port        Vlans allowed on trunk
Et0/0       20,30,40
Et1/0       20,30,40

Port        Vlans allowed and active in management domain
Et0/0       20,30,40
Et1/0       20,30,40

Port        Vlans in spanning tree forwarding state and not pruned
Et0/0       20,30,40
Et1/0       20,30,40

IOU2#show interfaces trunk

Port        Mode             Encapsulation  Status        Native vlan
Et0/0       on               802.1q         trunking      1

Port        Vlans allowed on trunk
Et0/0       20,30,40

Port        Vlans allowed and active in management domain
Et0/0       20,30,40

Port        Vlans in spanning tree forwarding state and not pruned
Et0/0       20,30,40
```

</details>

* Routeur

```cisco
R1#conf t
Enter configuration commands, one per line.  End with CNTL/Z.
R1(config)#interface ethernet 0/0.10
R1(config-subif)#encapsulation dot1Q 10
R1(config-subif)#ip address 10.3.10.254 255.255.255.0
R1(config-subif)#exit
R1(config)#interface ethernet 0/0.20
R1(config-subif)#encapsulation dot1Q 20
R1(config-subif)#ip address 10.3.20.254 255.255.255.0
R1(config-subif)#exit
R1(config)#interface ethernet 0/0.30
R1(config-subif)#encapsulation dot1Q 30
R1(config-subif)#ip address 10.3.30.254 255.255.255.0
R1(config-subif)#exit
R1(config)#interface ethernet 0/0.40
R1(config-subif)#encapsulation dot1Q 40
R1(config-subif)#ip address 10.3.40.254 255.255.255.0
R1(config-subif)#exit
R1(config)#interface ether 0/0
R1(config-if)#no shut
R1(config-if)#exit 
R1(config)#exit 
```

Pour vérifier `show ip int br`

<details>
<summary>Détails</summary>

```cisco
R1#show ip int br
Interface                  IP-Address      OK? Method Status                Protocol
Ethernet0/0                unassigned      YES unset  up                    up
Ethernet0/0.10             10.3.10.254     YES manual up                    up
Ethernet0/0.20             10.3.20.254     YES manual up                    up
Ethernet0/0.30             10.3.30.254     YES manual up                    up
Ethernet0/0.40             10.3.40.254     YES manual up                    up
Ethernet0/1                unassigned      YES unset  administratively down down
Ethernet0/2                unassigned      YES unset  administratively down down
Ethernet0/3                unassigned      YES unset  administratively down down
```
</details>

C'est bon la conf est terminé maintenant il ne nous reste plus qu'à tester voir si tout fonctionne comme on l'entend. Il faut que les `ping` respectent le petit tableau ci-dessous.

Réseaux | Client 10 |  Client 20 |  Client 30 |  Periph
--- | --- | --- | --- | ---
 `10` | ✅ | ❌ | ❌ | ❌
 `20` | ❌ | ✅ | ✅ | ✅
 `30` | ❌ | ✅ | ✅ | ✅
 `40` | ❌ | ✅ | ✅ | ✅

<details>
<summary>Pc 1</summary>

```bash
PC1> ping 10.3.10.1
10.3.10.1 icmp_seq=1 ttl=64 time=0.001 ms
10.3.10.1 icmp_seq=2 ttl=64 time=0.001 ms
10.3.10.1 icmp_seq=3 ttl=64 time=0.001 ms
10.3.10.1 icmp_seq=4 ttl=64 time=0.001 ms
10.3.10.1 icmp_seq=5 ttl=64 time=0.001 ms

PC1> ping 10.3.40.1
host (10.3.10.254) not reachable

PC1> ping 10.3.20.2
host (10.3.10.254) not reachable

PC1> ping 10.3.20.3
host (10.3.10.254) not reachable

PC1> ping 10.3.30.4
host (10.3.10.254) not reachable
```

</details>

<details>
<summary>Pc 2</summary>

```cisco
PC2> ping 10.3.10.1
10.3.10.1 icmp_seq=1 timeout
10.3.10.1 icmp_seq=2 timeout
10.3.10.1 icmp_seq=3 timeout
10.3.10.1 icmp_seq=4 timeout
10.3.10.1 icmp_seq=5 timeout

PC2> ping 10.3.20.2
10.3.20.2 icmp_seq=1 ttl=64 time=0.001 ms
10.3.20.2 icmp_seq=2 ttl=64 time=0.001 ms
10.3.20.2 icmp_seq=3 ttl=64 time=0.001 ms
10.3.20.2 icmp_seq=4 ttl=64 time=0.001 ms
10.3.20.2 icmp_seq=5 ttl=64 time=0.001 ms

PC2> ping 10.3.20.3
84 bytes from 10.3.20.3 icmp_seq=1 ttl=64 time=0.601 ms
84 bytes from 10.3.20.3 icmp_seq=2 ttl=64 time=0.640 ms
84 bytes from 10.3.20.3 icmp_seq=3 ttl=64 time=0.627 ms
84 bytes from 10.3.20.3 icmp_seq=4 ttl=64 time=1.116 ms
84 bytes from 10.3.20.3 icmp_seq=5 ttl=64 time=0.929 ms

PC2> ping 10.3.30.4
10.3.30.4 icmp_seq=1 timeout
10.3.30.4 icmp_seq=2 timeout
84 bytes from 10.3.30.4 icmp_seq=3 ttl=63 time=17.341 ms
84 bytes from 10.3.30.4 icmp_seq=4 ttl=63 time=11.538 ms
84 bytes from 10.3.30.4 icmp_seq=5 ttl=63 time=11.903 ms

PC2> ping 10.3.40.1
10.3.40.1 icmp_seq=1 timeout
10.3.40.1 icmp_seq=2 timeout
84 bytes from 10.3.40.1 icmp_seq=3 ttl=63 time=13.471 ms
84 bytes from 10.3.40.1 icmp_seq=4 ttl=63 time=19.927 ms
84 bytes from 10.3.40.1 icmp_seq=5 ttl=63 time=11.815 ms
```

</details>

<details>
<summary>Pc 3</summary>

```cisco
PC3> ping 10.3.10.1
10.3.10.1 icmp_seq=1 timeout
10.3.10.1 icmp_seq=2 timeout
10.3.10.1 icmp_seq=3 timeout
10.3.10.1 icmp_seq=4 timeout
10.3.10.1 icmp_seq=5 timeout

PC3> ping 10.3.20.2
84 bytes from 10.3.20.2 icmp_seq=1 ttl=64 time=0.523 ms
84 bytes from 10.3.20.2 icmp_seq=2 ttl=64 time=0.642 ms
84 bytes from 10.3.20.2 icmp_seq=3 ttl=64 time=0.866 ms
84 bytes from 10.3.20.2 icmp_seq=4 ttl=64 time=0.981 ms
84 bytes from 10.3.20.2 icmp_seq=5 ttl=64 time=0.715 ms

PC3> ping 10.3.20.3
10.3.20.3 icmp_seq=1 ttl=64 time=0.001 ms
10.3.20.3 icmp_seq=2 ttl=64 time=0.001 ms
10.3.20.3 icmp_seq=3 ttl=64 time=0.001 ms
10.3.20.3 icmp_seq=4 ttl=64 time=0.001 ms
10.3.20.3 icmp_seq=5 ttl=64 time=0.001 ms

PC3> ping 10.3.30.4
10.3.30.4 icmp_seq=1 timeout
10.3.30.4 icmp_seq=2 timeout
84 bytes from 10.3.30.4 icmp_seq=3 ttl=63 time=15.230 ms
84 bytes from 10.3.30.4 icmp_seq=4 ttl=63 time=19.814 ms
84 bytes from 10.3.30.4 icmp_seq=5 ttl=63 time=16.615 ms

PC3> ping 10.3.40.1
10.3.40.1 icmp_seq=1 timeout
10.3.40.1 icmp_seq=2 timeout
84 bytes from 10.3.40.1 icmp_seq=3 ttl=63 time=19.287 ms
84 bytes from 10.3.40.1 icmp_seq=4 ttl=63 time=16.380 ms
84 bytes from 10.3.40.1 icmp_seq=5 ttl=63 time=17.188 ms
```

</details>

<details>
<summary>Pc 4</summary>

```cisco
PC4> ping 10.3.10.1
10.3.10.1 icmp_seq=1 timeout
10.3.10.1 icmp_seq=2 timeout
10.3.10.1 icmp_seq=3 timeout
10.3.10.1 icmp_seq=4 timeout
10.3.10.1 icmp_seq=5 timeout

PC4> ping 10.3.20.2
10.3.20.2 icmp_seq=1 timeout
10.3.20.2 icmp_seq=2 timeout
84 bytes from 10.3.20.2 icmp_seq=3 ttl=63 time=17.805 ms
84 bytes from 10.3.20.2 icmp_seq=4 ttl=63 time=17.207 ms
84 bytes from 10.3.20.2 icmp_seq=5 ttl=63 time=16.689 ms

PC4> ping 10.3.20.3
10.3.20.3 icmp_seq=1 timeout
10.3.20.3 icmp_seq=2 timeout
84 bytes from 10.3.20.3 icmp_seq=3 ttl=63 time=18.249 ms
84 bytes from 10.3.20.3 icmp_seq=4 ttl=63 time=16.343 ms
84 bytes from 10.3.20.3 icmp_seq=5 ttl=63 time=16.591 ms

PC4> ping 10.3.30.4
10.3.30.4 icmp_seq=1 ttl=64 time=0.001 ms
10.3.30.4 icmp_seq=2 ttl=64 time=0.001 ms
10.3.30.4 icmp_seq=3 ttl=64 time=0.001 ms
10.3.30.4 icmp_seq=4 ttl=64 time=0.001 ms
10.3.30.4 icmp_seq=5 ttl=64 time=0.001 ms

PC4> ping 10.3.40.1
84 bytes from 10.3.40.1 icmp_seq=1 ttl=63 time=19.982 ms
84 bytes from 10.3.40.1 icmp_seq=2 ttl=63 time=15.395 ms
84 bytes from 10.3.40.1 icmp_seq=3 ttl=63 time=17.148 ms
84 bytes from 10.3.40.1 icmp_seq=4 ttl=63 time=16.713 ms
84 bytes from 10.3.40.1 icmp_seq=5 ttl=63 time=19.007 ms
```

</details>

<details>
<summary>Imprimante</summary>

```cisco
P1> ping 10.3.10.1
10.3.10.1 icmp_seq=1 timeout
10.3.10.1 icmp_seq=2 timeout
10.3.10.1 icmp_seq=3 timeout
10.3.10.1 icmp_seq=4 timeout
10.3.10.1 icmp_seq=5 timeout

P1> ping 10.3.20.2
84 bytes from 10.3.20.2 icmp_seq=1 ttl=63 time=20.068 ms
84 bytes from 10.3.20.2 icmp_seq=2 ttl=63 time=17.733 ms
84 bytes from 10.3.20.2 icmp_seq=3 ttl=63 time=16.333 ms
84 bytes from 10.3.20.2 icmp_seq=4 ttl=63 time=16.284 ms
84 bytes from 10.3.20.2 icmp_seq=5 ttl=63 time=16.608 ms

P1> ping 10.3.20.3
84 bytes from 10.3.20.3 icmp_seq=1 ttl=63 time=12.839 ms
84 bytes from 10.3.20.3 icmp_seq=2 ttl=63 time=15.055 ms
84 bytes from 10.3.20.3 icmp_seq=3 ttl=63 time=18.063 ms
84 bytes from 10.3.20.3 icmp_seq=4 ttl=63 time=12.444 ms
84 bytes from 10.3.20.3 icmp_seq=5 ttl=63 time=13.301 ms

P1> ping 10.3.30.4
84 bytes from 10.3.30.4 icmp_seq=1 ttl=63 time=11.519 ms
84 bytes from 10.3.30.4 icmp_seq=2 ttl=63 time=19.315 ms
84 bytes from 10.3.30.4 icmp_seq=3 ttl=63 time=17.600 ms
84 bytes from 10.3.30.4 icmp_seq=4 ttl=63 time=17.121 ms
84 bytes from 10.3.30.4 icmp_seq=5 ttl=63 time=17.349 ms

P1> ping 10.3.40.1
10.3.40.1 icmp_seq=1 ttl=64 time=0.001 ms
10.3.40.1 icmp_seq=2 ttl=64 time=0.001 ms
10.3.40.1 icmp_seq=3 ttl=64 time=0.001 ms
10.3.40.1 icmp_seq=4 ttl=64 time=0.001 ms
10.3.40.1 icmp_seq=5 ttl=64 time=0.001 ms
```

</details>

||Eh voilà tout fonctionne||

```
▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
█░░░░░░░░▀█▄▀▄▀██████░▀█▄▀▄▀██████
░░░░ ░░░░░░░▀█▄█▄███▀░░░ ▀█▄█▄███
```

## II. Cas Concret

Infrastructure monté

![inframonte](https://gitlab.com/Genisys/ccna/raw/master/Img/Infra_monte.png)

Création des Vlans sur les Switch.
```cisco
IOUx(config)#vlan 10
IOUx(config-vlan)#name Serveur
IOUx(config-vlan)#exit
IOUx(config)#vlan 20
IOUx(config-vlan)#name Admin
IOUx(config-vlan)#exit
IOUx(config)#vlan 30
IOUx(config-vlan)#name User
IOUx(config-vlan)#exit
IOUx(config)#vlan 40
IOUx(config-vlan)#name Print
IOUx(config-vlan)#exit
IOUx(config)#vlan 50
IOUx(config-vlan)#name Stagiaire
IOUx(config-vlan)#exit
IOUx(config)#vlan 70
IOUx(config-vlan)#name SS
IOUx(config-vlan)#exit
```

Pour faire la configuration des machines il vous faut faire les commandes ci-dessous tout en respectant la tableau d'adressage. 

Vlan | Nom Vlan
--- | --- 
10 | Serveur 
20 | Admin
30 | User
40 | Print
50 | Stagiaire
70 | SS

```cisco
IOU1(config)#interface ethernet 3/0        # On choisit l'interface
IOU1(config-if)#switchport mode access     # On met le mode access
IOU1(config-if)#switchport access vlan 30  # On lui donne l'accés au Vlan 30
IOU1(config-if)#exit
```

Si tu vois ça, il est fort probable que j'ai leave suite au nombreux crash de mon routeur m'obligeant à refaire toute la conf, crash du à l'import à chaque nouveau crash. (Enjoy Gns3). Sinon j'ai eu la motivation de tout refaire. Mon idée de base était de faire des access listes, solutions trouvé en cherchant pas mal sur l'internet français touvé [ici](https://www.ciscomadesimple.be/2015/07/28/vlan-access-list-vacl/), d'ailleurs le site est vraiment sympa même si il date j'ai lu d'autre "article" que celui-ci. M'enfin en espérant que tu vois pas ce texte et que je me suis chauffé à terminer ce tp. 
